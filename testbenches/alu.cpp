/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Valu.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Valu>* tb = new Testbench<Valu>("alu.vcd");
  Valu* top = tb->get_module();

  // TODO: add more tests
  const uint32_t N_TESTS          = 1;
  const char*    MNEMONIC[]       = {"ADD"};
  const uint32_t OPCODE[N_TESTS]  = {0x11};
  const uint32_t INPUT_A[N_TESTS] = {0x2};
  const uint32_t INPUT_B[N_TESTS] = {0x1};
  const uint32_t RESULT[N_TESTS]  = {0x3};

  tb->describe("Null output");
  tb->ASSERT_EQ(top->o_result, 0x0);

  for (unsigned int i = 0; i < N_TESTS; i++) {
    top->i_input_a = INPUT_A[i];
    top->i_input_b = INPUT_B[i];
    top->i_opcode  = OPCODE[i];
    tb->tick();

    tb->describe(MNEMONIC[i]);
    tb->ASSERT_EQ(top->o_result, RESULT[i]);
  }

  // TODO: test comparisons

  END_TESTBENCH(tb)
}
