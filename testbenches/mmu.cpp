/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vmmu.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vmmu>* tb = new Testbench<Vmmu>("mmu.vcd");
  Vmmu* top = tb->get_module();

  tb->tick();

  // tb->ASSERT_EQ(top->o_ram_addr, 0);
  // tb->ASSERT_EQ(top->io_ram_data, 0);
  // tb->ASSERT_EQ(top->o_ram_write_en_n, 1);
  // tb->ASSERT_EQ(top->o_ram_output_en_n, 1);
  // // tb->ASSERT_EQ(top->o_ram_chip_select_n, 0);
  // tb->ASSERT_EQ(top->o_output_word, 0);
  // tb->ASSERT_EQ(top->o_output_valid, 0);

  // top->i_input_addr = 0x42;
  // top->i_input_read_en = 1;

  // tb->tick();

  // tb->ASSERT_EQ(top->o_ram_addr, 0x42);
  // tb->ASSERT_EQ(top->io_ram_data, 0);
  // tb->ASSERT_EQ(top->o_ram_write_en_n, 1);
  // tb->ASSERT_EQ(top->o_ram_output_en_n, 1);
  // // tb->ASSERT_EQ(top->o_ram_chip_select_n, 0);
  // tb->ASSERT_EQ(top->o_output_word, 0);
  // tb->ASSERT_EQ(top->o_output_valid, 0);

  // tb->tick();

  // tb->ASSERT_EQ(top->o_ram_addr, 0x42);
  // tb->ASSERT_EQ(top->io_ram_data, 0);
  // tb->ASSERT_EQ(top->o_ram_write_en_n, 1);
  // tb->ASSERT_EQ(top->o_ram_output_en_n, 0);
  // // tb->ASSERT_EQ(top->o_ram_chip_select_n, 0);
  // tb->ASSERT_EQ(top->o_output_word, 0);
  // tb->ASSERT_EQ(top->o_output_valid, 0);

  // top->io_ram_data = 42;
  // tb->tick();

  // tb->ASSERT_EQ(top->o_ram_addr, 0x42);
  // tb->ASSERT_EQ(top->io_ram_data, 42);
  // tb->ASSERT_EQ(top->o_ram_write_en_n, 1);
  // tb->ASSERT_EQ(top->o_ram_output_en_n, 0);
  // // tb->ASSERT_EQ(top->o_ram_chip_select_n, 0);
  // tb->ASSERT_EQ(top->o_output_word, 42);
  // tb->ASSERT_EQ(top->o_output_valid, 1);

  END_TESTBENCH(tb)
}
