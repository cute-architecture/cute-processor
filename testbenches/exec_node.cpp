/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vexec_node.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vexec_node>* tb = new Testbench<Vexec_node>("exec_node.vcd");
  Vexec_node* top = tb->get_module();

  const unsigned int PIPELINE_DEPTH = 5;
  // Number of stages from decode to FIFO (register read stage)
  const unsigned int FIFO_DISTANCE = 2;

  top->i_discard = 0;

  tb->tick();

  // Program (first instruction last):
  // 32'h4020001c, // STR    L4, L2
  // 32'h0440000c, // WRITE  R2, L2
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h34100010, // ADD    L4, L3, L1
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h23000e24, // ANDI   L3, L2, #14
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h22000a38, // XORI   L2, L2, #10
  // 32'h0000000a, // B      #0
  // 32'h10002a3c, // STRI   L1, #42
  // 32'h0400082c, // WRITEI R2, #8
  // // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // // 32'h12000228, // SUBI   L2, L1, #2
  // 32'h00000030, // ADDI   L0, L0, #0 (NOP)
  // 32'h01000530  // ADDI   L1, L0, #5

  const unsigned int INSTRUCTION_NB = 20;

  tb->ASSERT_EQ(top->o_instruction_to_fetch_offset, 0);

  tb->ASSERT_EQ(top->o_write_fifo_data_available, 0);
  tb->ASSERT_EQ(top->o_write_fifo_output, 0);
  tb->ASSERT_EQ(top->o_str_fifo_data_available, 0);
  tb->ASSERT_EQ(top->o_str_fifo_output, 0);
  // tb->ASSERT_EQ(top->o_next_bid, 0);

  tb->tick(INSTRUCTION_NB + PIPELINE_DEPTH);

  // Check next_bid
  // tb->ASSERT_EQ(top->o_next_bid, 42);

  // Check FIFO outputs
  // tb->ASSERT_EQ(top->o_write_fifo_data_available, 1);
  tb->ASSERT_EQ(top->o_write_fifo_output, 0);
  // tb->ASSERT_EQ(top->o_str_fifo_data_available, 1);
  tb->ASSERT_EQ(top->o_str_fifo_output, 0);

  // STR FIFO
  // top->i_str_fifo_out_en = 1;
  tb->tick();

  // tb->ASSERT_EQ(top->o_write_fifo_data_available, 1);
  tb->ASSERT_EQ(top->o_write_fifo_output, 0);
  // tb->ASSERT_EQ(top->o_str_fifo_data_available, 1);
  // tb->ASSERT_EQ(top->o_str_fifo_output, 0x000000050000002a);

  tb->tick();

  // tb->ASSERT_EQ(top->o_write_fifo_data_available, 1);
  tb->ASSERT_EQ(top->o_write_fifo_output, 0);
  tb->ASSERT_EQ(top->o_str_fifo_data_available, 0);
  // tb->ASSERT_EQ(top->o_str_fifo_output, 0x0000000f0000000a);

  // top->i_str_fifo_out_en = 0;
  tb->tick();

  // WRITE FIFO
  // top->i_write_fifo_out_en = 1;
  tb->tick();
  // tb->ASSERT_EQ(top->o_write_fifo_data_available, 1);
  // tb->ASSERT_EQ(top->o_write_fifo_output, 0x0200000008);
  tb->ASSERT_EQ(top->o_str_fifo_data_available, 0);

  tb->tick();

  tb->ASSERT_EQ(top->o_write_fifo_data_available, 0);
  // tb->ASSERT_EQ(top->o_write_fifo_output, 0x020000000a);
  tb->ASSERT_EQ(top->o_str_fifo_data_available, 0);

  END_TESTBENCH(tb)
}
