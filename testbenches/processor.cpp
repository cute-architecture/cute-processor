/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vprocessor.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vprocessor>* tb = new Testbench<Vprocessor>("processor.vcd");
  Vprocessor* top = tb->get_module();

  // const unsigned int BIT_LENGTH = 50604;
  const unsigned int BIT_LENGTH = 3000;

  const char UART_CHARS_1_NB = 3;
  char UART_CHARS_1[UART_CHARS_1_NB] = {
    0x32,
    0x34, 0x32
  };

  const char UART_CHARS_2_NB = 7;
  char UART_CHARS_2[UART_CHARS_2_NB] = {
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34
  };

  const char UART_CHARS_3_NB = 1 + 2*12;
  char UART_CHARS_3[UART_CHARS_3_NB] = {
    0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
    0x34, 0x32,
  };

  tb->tick();

  // TODO: add a mean to group asserts
  tb->describe("IDLE");
  for (unsigned int i = 0; i < BIT_LENGTH + 2047; i++) {
    // tb->ASSERT_EQ(top->o_uart_tx, 1);
    tb->tick();
  }

  for (unsigned int c = 0; c < UART_CHARS_1_NB; c++) {
    tb->describe("Start bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 0);
      tb->tick();
    }

    tb->describe("Char");
    for (unsigned int j = 0; j < 8; j++) {
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, (UART_CHARS_1[c] >> j) & 0x1);
        tb->tick();
      }
    }

    tb->describe("Stop bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 1);
      tb->tick();
    }
  }

  tb->describe("IDLE");
  for (unsigned int i = 0; i < BIT_LENGTH; i++) {
    // tb->ASSERT_EQ(top->o_uart_tx, 1);
    tb->tick();
  }

  for (unsigned int c = 0; c < UART_CHARS_2_NB; c++) {
    tb->describe("Start bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 0);
      tb->tick();
    }

    tb->describe("Char");
    for (unsigned int j = 0; j < 8; j++) {
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, (UART_CHARS_2[c] >> j) & 0x1);
        tb->tick();
      }
    }

    tb->describe("Stop bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 1);
      tb->tick();
    }
  }

  for (unsigned int c = 0; c < UART_CHARS_3_NB; c++) {
    if (c % 2 == 0) {
      tb->describe("IDLE");
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, 1);
        tb->tick();
      }

      tb->describe("IDLE");
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, 1);
        tb->tick();
      }

      tb->describe("IDLE");
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, 1);
        tb->tick();
      }
    }

    tb->describe("Start bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 0);
      tb->tick();
    }

    tb->describe("Char");
    for (unsigned int j = 0; j < 8; j++) {
      for (unsigned int i = 0; i < BIT_LENGTH; i++) {
        // tb->ASSERT_EQ(top->o_uart_tx, (UART_CHARS_3[c] >> j) & 0x1);
        tb->tick();
      }
    }

    tb->describe("Stop bit");
    for (unsigned int i = 0; i < BIT_LENGTH; i++) {
      // tb->ASSERT_EQ(top->o_uart_tx, 1);
      tb->tick();
    }
  }

  END_TESTBENCH(tb)
}
