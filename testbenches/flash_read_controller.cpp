/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vflash_read_controller.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vflash_read_controller>* tb
    = new Testbench<Vflash_read_controller>("flash_read_controller.vcd");
  Vflash_read_controller* top = tb->get_module();

  const int CLK_DIV = 2;
  const int SPI_CLK_RATIO = 2; // /!\ SPI clock is f(i_clk) / CLK_RATIO / 2
  const int WORD_SIZE = 8;
  const int ADDRESS_LENGTH = 24;

  // FIXME: add asserts to this testbench

  tb->tick(SPI_CLK_RATIO*CLK_DIV);

  top->i_input_valid = 1;

  tb->tick(SPI_CLK_RATIO*CLK_DIV*(WORD_SIZE + ADDRESS_LENGTH) + 5);

  unsigned char flash_data[] = {1, 0, 1, 0, 0, 0, 1, 1};

  for (unsigned int i = 0; i < WORD_SIZE; i++) {
    top->i_spi_miso = flash_data[i];

    tb->tick(SPI_CLK_RATIO*CLK_DIV);
  }

  unsigned char flash_data2[] = {1, 1, 0, 0, 1, 0, 1, 0};

  for (unsigned int i = 0; i < WORD_SIZE; i++) {
    top->i_spi_miso = flash_data2[i];

    tb->tick(4);
  }

  top->i_spi_miso = 0;

  tb->tick(SPI_CLK_RATIO*CLK_DIV);

  END_TESTBENCH(tb)
}
