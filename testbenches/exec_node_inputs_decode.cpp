/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vexec_node_inputs_decode.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vexec_node_inputs_decode>* tb
    = new Testbench<Vexec_node_inputs_decode>("exec_node_inputs_decode.vcd");
  Vexec_node_inputs_decode* top = tb->get_module();

  // TODO: add more tests

  tb->describe("Null output");

  tb->ASSERT_EQ(top->o_opcode, 0);
  tb->ASSERT_EQ(top->o_addr_local, 0);
  tb->ASSERT_EQ(top->o_addr_global, 0);

  tb->describe("READ L2, R1");

  top->i_instruction = 0x08000012;
  tb->tick();

  tb->ASSERT_EQ(top->o_opcode, 0b00001);
  tb->ASSERT_EQ(top->o_addr_local, 2);
  tb->ASSERT_EQ(top->o_addr_global, 1);

  // TODO: test the LDR instruction decoding

  tb->describe("HEND");
  top->i_instruction = 0xf8000000;
  tb->tick();

  tb->describe("Reset");
  top->i_reset = 1;
  tb->tick();
  top->i_reset = 0;

  END_TESTBENCH(tb)
}

