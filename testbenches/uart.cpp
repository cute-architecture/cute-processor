/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vuart.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vuart>* tb = new Testbench<Vuart>("uart.vcd");
  Vuart* top = tb->get_module();

  const unsigned int CLK_DIV = 10417;
  const unsigned int WORD_SIZE = 8;

  tb->tick(CLK_DIV);

  tb->describe("Idle state");
  tb->ASSERT_EQ(top->o_ready, 1);
  tb->ASSERT_EQ(top->o_uart_tx, 1);

  const char input_tx_array[] = {1, 0, 1, 0, 0, 0, 1, 0};
  top->i_input_tx = 0b01000101;
  top->i_input_valid = 1;

  // The transmission only begins on the UART internal clock rising edge
  tb->tick(CLK_DIV);

  tb->ASSERT_EQ(top->o_ready, 0);
  tb->ASSERT_EQ(top->o_uart_tx, 1);

  tb->tick(CLK_DIV);

  tb->describe("Start bit");
  tb->ASSERT_EQ(top->o_ready, 0);
  tb->ASSERT_EQ(top->o_uart_tx, 0);

  // Test latching of input: this must not affect the current frame
  tb->describe("Latching of input");
  top->i_input_tx = 0b11111111;

  for (unsigned int i = 0; i < WORD_SIZE; i++) {
    tb->tick(CLK_DIV);

    tb->ASSERT_EQ(top->o_ready, 0);
    tb->ASSERT_EQ(top->o_uart_tx, input_tx_array[i]);
  }

  tb->tick(CLK_DIV);

  tb->describe("Stop bit");
  tb->ASSERT_EQ(top->o_ready, 1);
  tb->ASSERT_EQ(top->o_uart_tx, 1);

  // TODO: test another frame

  END_TESTBENCH(tb)
}
