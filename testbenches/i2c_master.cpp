/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vi2c_master.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vi2c_master>* tb = new Testbench<Vi2c_master>("i2c_master.vcd");
  Vi2c_master* top = tb->get_module();

  const unsigned int CLK_DIV = 3;

  top->i_slave_address_rw = 0xaa | 0x01;
  top->i_slave_data = 0xaa;

  tb->tick(3*CLK_DIV);

  top->i_input_valid = 1;

  tb->tick(250);

  // tb->ASSERT_EQ(top->io_i2c_scl, 1);
  // tb->ASSERT_EQ(top->io_i2c_sda, 1);

  END_TESTBENCH(tb)
}
