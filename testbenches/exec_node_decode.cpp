/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vexec_node_decode.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vexec_node_decode>* tb
    = new Testbench<Vexec_node_decode>("exec_node_decode.vcd");
  Vexec_node_decode* top = tb->get_module();

  // TODO: add more tests

  tb->describe("ADDI   L3, L2, #1");
  top->i_instruction = 0x0c000132;
  tb->tick();

  tb->ASSERT_EQ(top->o_opcode, 0b00001);
  tb->ASSERT_EQ(top->o_imm_value, 1);
  tb->ASSERT_EQ(top->o_imm_upper_half, 0);
  tb->ASSERT_EQ(top->o_addr_b, 2);
  tb->ASSERT_EQ(top->o_addr_d, 3);
  tb->ASSERT_EQ(top->o_reg_write_en, 1);

  tb->ASSERT_EQ(top->o_branch_block_index, 0);
  tb->ASSERT_EQ(top->o_branch_block_index_valid, 0);
  tb->ASSERT_EQ(top->o_block_end, 0);
  tb->ASSERT_EQ(top->o_write_fifo_in_en, 0);
  tb->ASSERT_EQ(top->o_str_fifo_in_en, 0);

  tb->describe("B      #2");
  top->i_instruction = 0x50000002;
  tb->tick();

  tb->ASSERT_EQ(top->o_opcode, 0b01010);
  tb->ASSERT_EQ(top->o_branch_block_index, 2);
  tb->ASSERT_EQ(top->o_branch_block_index_valid, 1);

  tb->ASSERT_EQ(top->o_block_end, 0);
  tb->ASSERT_EQ(top->o_reg_write_en, 0);
  tb->ASSERT_EQ(top->o_write_fifo_in_en, 0);
  tb->ASSERT_EQ(top->o_str_fifo_in_en, 0);

  tb->describe("WRITE  R1, L2");
  top->i_instruction = 0x30000201;
  tb->tick();

  tb->ASSERT_EQ(top->o_opcode, 0b00110);
  tb->ASSERT_EQ(top->o_addr_a, 2);
  tb->ASSERT_EQ(top->o_addr_w, 1);
  tb->ASSERT_EQ(top->o_write_fifo_in_en, 1);

  tb->ASSERT_EQ(top->o_block_end, 0);
  tb->ASSERT_EQ(top->o_reg_write_en, 0);
  tb->ASSERT_EQ(top->o_str_fifo_in_en, 0);

  tb->describe("BEND");
  top->i_instruction = 0xf8000000;
  tb->tick();

  tb->ASSERT_EQ(top->o_opcode, 0b11111);
  tb->ASSERT_EQ(top->o_block_end, 1);

  tb->ASSERT_EQ(top->o_reg_write_en, 0);
  tb->ASSERT_EQ(top->o_write_fifo_in_en, 0);
  tb->ASSERT_EQ(top->o_str_fifo_in_en, 0);

  END_TESTBENCH(tb)
}
