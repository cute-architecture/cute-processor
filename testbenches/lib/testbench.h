/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "verilated_vcd_c.h"
#include "verilated_cov.h"

#define RESET       "\033[0m"
#define BLACK       "\033[30m"        // Black
#define RED         "\033[31m"        // Red
#define GREEN       "\033[32m"        // Green
#define YELLOW      "\033[33m"        // Yellow
#define BLUE        "\033[34m"        // Blue
#define MAGENTA     "\033[35m"        // Magenta
#define CYAN        "\033[36m"        // Cyan
#define WHITE       "\033[37m"        // White
#define BOLDBLACK   "\033[1m\033[30m" // Bold Black
#define BOLDRED     "\033[1m\033[31m" // Bold Red
#define BOLDGREEN   "\033[1m\033[32m" // Bold Green
#define BOLDYELLOW  "\033[1m\033[33m" // Bold Yellow
#define BOLDBLUE    "\033[1m\033[34m" // Bold Blue
#define BOLDMAGENTA "\033[1m\033[35m" // Bold Magenta
#define BOLDCYAN    "\033[1m\033[36m" // Bold Cyan
#define BOLDWHITE   "\033[1m\033[37m" // Bold White

#define VNAME(x) #x
#define ASSERT_EQ(a, b) assert_eq(a, b, VNAME(a), __FILE__, __LINE__)
// TODO: rewrite this without a macro
#define END_TESTBENCH(tb) {\
  unsigned int failed_test_counter = tb->get_failed_test_counter();\
  delete tb;\
  if (failed_test_counter)\
    exit(EXIT_FAILURE);\
  exit(EXIT_SUCCESS);\
}

namespace testbench {
  void print_hex(long int hex);
  void print_dec(long int dec);

  template<class VModule>
  class Testbench {
    private:
      unsigned int assert_counter = 0;
      unsigned int failed_test_counter = 0;
      VModule* module;
      VerilatedVcdC* trace;
      unsigned long int tick_count = 0;
      const char* current_test_description = "";
      unsigned int current_passed_test_counter = 0;

      void print_current_passed_test() {
        if (current_passed_test_counter) {
          printf(GREEN "[OK] %s × %d" RESET "\n",
                 current_test_description, current_passed_test_counter);
        }
      }

    public:
      Testbench(const char* trace_filepath) {
        module = new VModule;

        Verilated::traceEverOn(true);
        trace = new VerilatedVcdC;
        module->trace(trace, 99);
        trace->open(trace_filepath);

        printf("Running simulation for %s…\n", trace_filepath);

        // The module clock must be exposed with `i_clk`
        module->i_clk = 1;
      }

      ~Testbench() {
        trace->close();
        VerilatedCov::write("coverage.dat");
        delete trace;
        delete module;

        print_summary();
      }

      VModule* get_module() {
        return module;
      }

      unsigned int get_failed_test_counter() {
        return failed_test_counter;
      }

      unsigned int get_assert_counter() {
        return assert_counter;
      }

      void describe(const char* description) {
        print_current_passed_test();

        current_test_description = description;
        current_passed_test_counter = 0;
      }

      void print_summary() {
        print_current_passed_test();

        printf("Simulated %d ticks.\n", tick_count);

        if (failed_test_counter)
          printf(RED
                 "%d of %d assertion(s) failed."
                 RESET "\n",
                 failed_test_counter, assert_counter);
        else
          printf(GREEN
                 "Success! No assertion failed (%d assertion(s) tested)."
                 RESET "\n",
                 assert_counter);
      }

      void tick(unsigned long int nticks = 1) {
        for (unsigned long int i = 0; i < nticks; i++) {
          for (unsigned int clk = 0; clk < 2; clk++) {
            trace->dump(2*tick_count + clk);
            // The module clock must be exposed with `i_clk`
            module->i_clk = !module->i_clk;
            module->eval();
          }

          tick_count++;
        }
      }

      void assert_eq(long int a, long int b,
                     const char* left_name,
                     const char* file, unsigned int line) {
        assert_counter++;

        if (a == b) {
          current_passed_test_counter++;
          return;
        }

        print_current_passed_test();

        current_passed_test_counter = 0;
        failed_test_counter++;

        printf(RED
               "[FAIL]"
               RESET
               " %s:%d: "
               "%s ("
               RED
               "0x%llx"
               RESET
               ") != 0x%llx\n",
               file, line, left_name, a, b);
      }
  };
}
