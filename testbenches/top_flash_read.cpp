/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vtop_flash_read.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vtop_flash_read>* tb = new Testbench<Vtop_flash_read>("top_flash_read.vcd");
  Vtop_flash_read* top = tb->get_module();

  const int UART_CLK_DIV = 10417;
  const int SPI_CLK_RATIO = 40; // /!\ SPI clock is f(i_clk) / CLK_RATIO / 2
  const int WORD_SIZE = 8;
  const int ADDRESS_LENGTH = 24;

  // Skip the chip select
  tb->tick(1.5*2*SPI_CLK_RATIO);

  // FIXME: update the testbench so that the miso generation timing becomes
  // right again

  tb->tick(1.5*2*SPI_CLK_RATIO);

  tb->tick(2*SPI_CLK_RATIO*ADDRESS_LENGTH);

  // unsigned char flash_data[] = {1, 0, 1, 0, 0, 0, 1, 1};
  unsigned char flash_data[] = {0, 1, 1, 0, 0, 1, 0, 1};

  for (unsigned int i = 0; i < WORD_SIZE; i++) {
    top->i_spi_miso = flash_data[i];

    tb->tick(2*SPI_CLK_RATIO);
  }

  // unsigned char flash_data2[] = {1, 1, 0, 0, 1, 0, 1, 0};

  // for (unsigned int i = 0; i < WORD_SIZE; i++) {
  //   top->spi_miso = flash_data2[i];

  //   tb->tick(SPI_CLK_RATIO*UART_CLK_DIV);
  // }

  top->i_spi_miso = 0;

  // Run start bit, data and stop bit
  tb->tick((2+WORD_SIZE)*UART_CLK_DIV);

  END_TESTBENCH(tb)
}
