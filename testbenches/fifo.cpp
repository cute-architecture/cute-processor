/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vfifo.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vfifo>* tb = new Testbench<Vfifo>("fifo.vcd");
  Vfifo* top = tb->get_module();

  const unsigned char FIFO_LENGTH = 4;
  unsigned int i_data_in[FIFO_LENGTH - 1] = {0x1, 0x2, 0x3};

  tb->describe("Null output");
  tb->ASSERT_EQ(top->o_data_out, 0);
  tb->ASSERT_EQ(top->o_data_available, 0);

  tb->describe("Write data to the FIFO");
  top->i_en_in = 1;
  for (unsigned int i = 0; i < FIFO_LENGTH - 1; i++) {
    top->i_data_in = i_data_in[i];

    tb->tick();
  }

  tb->ASSERT_EQ(top->o_data_out, i_data_in[0]);
  tb->ASSERT_EQ(top->o_data_available, 1);

  top->i_en_in = 0;
  top->i_en_out = 1;
  tb->tick();

  tb->describe("Read data from the FIFO");
  tb->tick();
  tb->ASSERT_EQ(top->o_data_out, i_data_in[1]);
  tb->ASSERT_EQ(top->o_last_item, 0);

  tb->tick();
  tb->ASSERT_EQ(top->o_data_out, i_data_in[2]);
  tb->ASSERT_EQ(top->o_last_item, 0);

  tb->ASSERT_EQ(top->o_data_available, 0);

  tb->describe("Reset the FIFO without stopping the output");
  top->i_reset = 1;
  tb->tick();
  top->i_reset = 0;
  tb->ASSERT_EQ(top->o_data_out, 0);

  tb->describe("Write data to the FIFO again");
  top->i_en_in = 1;
  for (unsigned int i = 0; i < FIFO_LENGTH - 1; i++) {
    top->i_data_in = i_data_in[i];

    tb->tick();
  }

  tb->ASSERT_EQ(top->o_data_out, i_data_in[0]);
  tb->ASSERT_EQ(top->o_data_available, 1);

  tb->describe("Reset the FIFO");
  top->i_reset = 1;
  tb->tick();
  top->i_reset = 0;

  tb->ASSERT_EQ(top->o_data_available, 0);

  // TODO: test what happens when there are more reads than writes
  // TODO: test what happens when en_in == 1 && en_out == 1

  END_TESTBENCH(tb)
}
