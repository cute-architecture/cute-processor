/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vregister_file.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vregister_file>* tb
    = new Testbench<Vregister_file>("register_file.vcd");
  Vregister_file* top = tb->get_module();

  top->i_write_en = 0;

  tb->describe("Null output");
  tb->ASSERT_EQ(top->o_output_a, 0);
  tb->ASSERT_EQ(top->o_output_b, 0);

  tb->tick();

  tb->describe("Write to the register");
  top->i_input_d = 0xDEADBEEF;
  top->i_addr_d = 3;
  top->i_write_en = 1;
  tb->tick();

  tb->ASSERT_EQ(top->o_output_a, 0);

  tb->describe("Read the register");
  top->i_addr_a = 3;
  top->i_write_en = 0;
  tb->tick();

  tb->ASSERT_EQ(top->o_output_a, 0xDEADBEEF);

  tb->describe("Read and write at the same time, to and from the same register");
  top->i_input_d = 0xB01DFACE;
  top->i_addr_d = 3;
  top->i_write_en = 1;
  top->i_addr_a = 3;
  tb->tick();

  tb->ASSERT_EQ(top->o_output_a, 0xDEADBEEF);

  top->i_addr_a = 3;
  top->i_write_en = 0;
  tb->tick();
  tb->ASSERT_EQ(top->o_output_a, 0xB01DFACE);

  END_TESTBENCH(tb)
}
