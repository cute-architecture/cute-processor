/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vscheduler_3xnodes.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vscheduler_3xnodes>* tb = new Testbench<Vscheduler_3xnodes>("scheduler_3xnodes.vcd");
  Vscheduler_3xnodes* top = tb->get_module();

  const unsigned int EXECUTION_NODE_NB = 4;

  // const unsigned char exec_priority[EXECUTION_NODE_NB] = {1, 3, 2, 0};
  // TODO: write a much more detailed testbench

  // top->o_exec_priority0 = 2;
  // top->o_exec_priority1 = 3;
  // top->o_exec_priority2 = 0;
  // top->o_exec_priority3 = 2;

  // top->i_next_bid0 = 42;
  // top->i_next_bid1 = 43;
  // top->i_next_bid2 = 44;
  // top->i_next_bid3 = 45;

  // top->i_next_block_exec_mode0 = 0;
  // top->i_next_block_exec_mode1 = 1;
  // top->i_next_block_exec_mode2 = 2;
  // top->i_next_block_exec_mode3 = 3;

  tb->tick(2);

  // tb->ASSERT_EQ(top->o_selected_next_bid, 44);
  // tb->ASSERT_EQ(top->o_selected_next_block_exec_mode, 2);

  END_TESTBENCH(tb)
}
