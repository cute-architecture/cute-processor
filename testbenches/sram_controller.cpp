/*
 * Copyright © Romain Fouquet, 2018-2020
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lib/testbench.h"
#include "Vsram_controller.h"

using namespace testbench;

int main(int argc, char **argv, char **env) {
  Verilated::commandArgs(argc, argv);

  Testbench<Vsram_controller>* tb = new Testbench<Vsram_controller>("sram_controller.vcd");
  Vsram_controller* top = tb->get_module();

  const unsigned int TEST_ADDR = 0x42;
  const unsigned int TEST_DATA = 0xca742ca7;
  const unsigned int TEST_DATA2 = 0xd0942d09;

  tb->tick(2);
  tb->ASSERT_EQ(top->o_output_word, 0);
  tb->ASSERT_EQ(top->o_output_valid, 0);

  // Write a test word
  top->i_addr = TEST_ADDR;
  top->i_data = TEST_DATA;
  top->i_rw = 1;
  top->i_valid = 1;
  tb->tick(1);
  tb->ASSERT_EQ(top->o_sram_addr, TEST_ADDR);
  tb->ASSERT_EQ(top->io_sram_data, TEST_DATA & 0xffff);
  tb->ASSERT_EQ(top->o_output_valid, 0);

  // Write a second test word to test input latching
  top->i_addr = TEST_ADDR;
  top->i_data = TEST_DATA2;
  top->i_rw = 1;
  top->i_valid = 1;

  // Check the upper half-word is requested to the SRAM chip
  top->i_valid = 0;
  tb->tick(1);
  tb->ASSERT_EQ(top->o_sram_addr, TEST_ADDR + 1);
  tb->ASSERT_EQ(top->io_sram_data, TEST_DATA >> 16);
  tb->ASSERT_EQ(top->o_output_valid, 0);

  // Wait some time
  tb->tick(7);

  // Read the previously written test word
  top->i_addr = TEST_ADDR;
  top->i_rw = 0;
  top->i_valid = 1;
  tb->tick(1);
  top->i_valid = 0;
  tb->tick(3);
  tb->ASSERT_EQ(top->o_output_word, TEST_DATA);
  tb->ASSERT_EQ(top->o_output_valid, 1);

  tb->tick(4);

  // TODO: more complex tests

  END_TESTBENCH(tb)
}
