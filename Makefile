DOC_FILE = technical_report.tex
DOC_DIR = doc/

doc: $(DOC_DIR)$(DOC_FILE)
	cd doc && \
	latexmk -xelatex $(DOC_FILE)
