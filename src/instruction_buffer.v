/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __INSTRUCTION_BUFFER_MODULE__
`define __INSTRUCTION_BUFFER_MODULE__

`include "isa_const.v"

module instruction_buffer #(
  parameter INSTRUCTION_SIZE = `INSTRUCTION_SIZE,
  parameter BUFFER_LENGTH    = `MAX_INPUTS_BODY_BLOCK_LENGTH
)(
  input  wire                             i_clk,
  input  wire                             i_write_en,
  input  wire [$clog2(BUFFER_LENGTH)-1:0] i_addr_r,
  input  wire [$clog2(BUFFER_LENGTH)-1:0] i_addr_w,
  input  wire [INSTRUCTION_SIZE-1:0]      i_instruction,
  output reg  [INSTRUCTION_SIZE-1:0]      o_instruction
);

  reg [INSTRUCTION_SIZE-1:0] buffer [BUFFER_LENGTH-1:0];

  always @(posedge i_clk)
  begin
    o_instruction <= buffer[i_addr_r];

    if (i_write_en)
    begin
      buffer[i_addr_w] <= i_instruction;
    end
  end

endmodule

`endif // __INSTRUCTION_BUFFER_MODULE__
