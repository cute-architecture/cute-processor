/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`ifndef __PROCESSOR_CONST__
`define __PROCESSOR_CONST__

`define EXECUTION_NODE_NB       3 // STUDY

// Clock
`define EXTERN_CLK_FREQ         100_000_000 // 100 MHz
`ifdef VERILATOR
  `define GLOBAL_CLK_DIV        4 // Simulation
`else
// FIXME
  `define GLOBAL_CLK_DIV        256 // For hardware, does not work if lower
`endif

// Peripheral-relative offsets
// FIXME: change these to real offsets e.g. 'h8000
`define GPIO_START_OFFSET_PARTIAL 'h2 // FIXME
`define UART_START_OFFSET_PARTIAL 'h3 // FIXME

// Flash
`define SPI_ADDRESS_SIZE        24
`define FLASH_BYTE_SIZE         8
`define FLASH_PROGRAM_OFFSET    'h0002_0fbc

// RAM
`define RAM_SIZE                512 * 1024 // 512 kiB
`define RAM_WORD_SIZE           16 // 16 bits
`define RAM_ADDR_SIZE           18

// GPIO
`define GPIO_NB                 2

// UART
`define UART_BITRATE            9600

`endif // __PROCESSOR_CONST__
