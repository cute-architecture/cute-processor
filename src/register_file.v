/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __REGISTER_FILE_MODULE__
`define __REGISTER_FILE_MODULE__

module register_file #(
  parameter WORD_SIZE   = 32,
  parameter REGISTER_NB = 16
)(
  input  wire                           i_clk,
  input  wire                           i_write_en,
  input  wire [$clog2(REGISTER_NB)-1:0] i_addr_a,
  input  wire [$clog2(REGISTER_NB)-1:0] i_addr_b,
  input  wire [$clog2(REGISTER_NB)-1:0] i_addr_d,
  input  wire [WORD_SIZE-1:0]           i_input_d,
  output reg  [WORD_SIZE-1:0]           o_output_a,
  output reg  [WORD_SIZE-1:0]           o_output_b
);

  reg  [WORD_SIZE-1:0] memory [REGISTER_NB-1:0];

  always @(posedge i_clk)
  begin
    o_output_a <= memory[i_addr_a];
    o_output_b <= memory[i_addr_b];

    if (i_write_en)
    begin
      memory[i_addr_d] <= i_input_d;
    end
  end

endmodule

`endif // __REGISTER_FILE_MODULE__
