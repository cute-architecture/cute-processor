/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __SCHEDULER_MODULE__
`define __SCHEDULER_MODULE__

`include "isa_const.v"
`include "processor_const.v"

`include "pack_unpack_2d_array.v"

`define UPDATE_MEM_INSTRUCTION_TO_FETCH(EXN_ID) \
o_mem_address \
  = `FLASH_START_OFFSET /* FIXME: this should not depend on the Flash, to be able to run from RAM */ \
  + `FLASH_PROGRAM_OFFSET \
  + {{ADDRESS_SIZE-BID_SIZE{1'b0}}, i_current_bid[EXN_ID][BID_SIZE-1:0]} /* FIXME: rename bid to block_offset; make the offset a word offset instead of a byte offset, forcing alignment of blocks*/ \
  + {{ADDRESS_SIZE-$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(INSTRUCTION_SIZE/BYTE_SIZE){1'b0}}, \
        i_instruction_to_fetch_offset[EXN_ID], \
     {$clog2(INSTRUCTION_SIZE/BYTE_SIZE){1'b0}}}; \
o_mem_bid = i_current_bid[EXN_ID]; \
o_mem_block_offset = i_instruction_to_fetch_offset[EXN_ID]; \
o_mem_instr_or_data = 0; /* Instruction */ \
o_mem_rw = 0; \
o_mem_valid = 1;
// TODO: acknowledge the exec node to enable pipelining

`define UPDATE_MEM_DATA_TO_READ(EXN_ID) \
o_mem_address = i_mem_data_to_read_address[EXN_ID]; \
o_mem_bid = 0; /* Does not matter */ \
o_mem_block_offset = 0; /* Does not matter */ \
o_mem_instr_or_data = 1; /* Data */ \
o_mem_rw = 0; /* Read */ \
o_mem_valid = 1;
// TODO: acknowledge the exec node to enable pipelining

// TODO: scale this according to EXECUTION_NODE_NB

module scheduler_3xnodes #(
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter INSTRUCTION_SIZE             = `INSTRUCTION_SIZE,
  parameter WORD_SIZE                    = `WORD_SIZE,
  parameter BYTE_SIZE                    = `BYTE_SIZE,
  parameter EXECUTION_NODE_NB            = `EXECUTION_NODE_NB,
  parameter MAX_SUBBLOCK_NB              = `MAX_SUBBLOCK_NB, // Max number a block can branch to
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH,
  parameter BID_SIZE                     = `BID_SIZE,
  parameter GLOBAL_REG_NB                = `GLOBAL_REG_NB,
  parameter EXEC_MODE_SIZE               = `EXEC_MODE_SIZE
)(
  input  wire                                                              i_clk,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_ready,
  input  wire [EXECUTION_NODE_NB*$clog2(MAX_SUBBLOCK_NB)-1:0]              i_p_next_block_index,
  input  wire [EXECUTION_NODE_NB*BID_SIZE-1:0]                             i_p_next_bid,
  input  wire [EXECUTION_NODE_NB*EXEC_MODE_SIZE-1:0]                       i_p_next_block_exec_mode,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_next_block_valid,
  input  wire [EXECUTION_NODE_NB*$clog2(MAX_SUBBLOCK_NB)-1:0]              i_p_branch_block_index,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_branch_block_index_valid,
  input  wire [EXECUTION_NODE_NB*BID_SIZE-1:0]                             i_p_current_bid,
  input  wire [EXECUTION_NODE_NB*$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_p_instruction_to_fetch_offset,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_instruction_to_fetch_valid,
  input  wire                                                              i_current_block_ended,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_discard_subblocks,
  input  wire [EXECUTION_NODE_NB-1:0]                                      i_request_global_reg_read,

  /* verilator lint_off UNUSED */
  input wire  [EXECUTION_NODE_NB*ADDRESS_SIZE-1:0]                         i_p_mem_data_to_read_address,
  /* verilator lint_on UNUSED */
  input wire  [EXECUTION_NODE_NB-1:0]                                      i_mem_data_to_read_valid,

  input  wire [EXECUTION_NODE_NB*$clog2(GLOBAL_REG_NB)-1:0]                i_p_inputs_global_addr,
  input  wire                                                              i_write_fifo_data_available_muxed,
  input  wire [ADDRESS_SIZE+WORD_SIZE-1:0]                                 i_str_fifo_output_muxed,
  input  wire                                                              i_str_fifo_data_available_muxed,

  output reg  [BID_SIZE-1:0]                                               o_mem_bid = 0,
  output reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0]                   o_mem_block_offset = 0,
  output reg  [ADDRESS_SIZE-1:0]                                           o_mem_address = 0,
  output reg  [WORD_SIZE-1:0]                                              o_mem_data = 0,
  output reg                                                               o_mem_instr_or_data = 0, // 0: instr, 1: data
  output reg                                                               o_mem_rw = 0, // 0: read, 1: write (only matters for data)
  output reg                                                               o_mem_valid = 0,

  output reg  [$clog2(EXECUTION_NODE_NB)-1:0]                              o_current_exn,
  output wire [EXECUTION_NODE_NB*$clog2(EXECUTION_NODE_NB)-1:0]            o_p_exec_priority,
  output reg  [$clog2(EXECUTION_NODE_NB)-1:0]                              o_exec_depth = 0, // FIXME: implement it
  output reg  [BID_SIZE-1:0]                                               o_selected_next_bid, // TODO: find a better word instead of 'selected'
  output reg  [EXECUTION_NODE_NB-1:0]                                      o_selected_next_valid,
  output reg  [EXECUTION_NODE_NB-1:0]                                      o_next_block_index_inc,
  output reg  [EXECUTION_NODE_NB-1:0]                                      o_discard_exn,
  output wire [EXECUTION_NODE_NB-1:0]                                      o_grant_global_reg_read,
  output reg                                                               o_global_reg_write_en,
  output reg  [$clog2(GLOBAL_REG_NB)-1:0]                                  o_global_reg_addr
);

  wire [EXECUTION_NODE_NB*(EXECUTION_NODE_NB-1)/2-1:0] priority_comparison;

  wire [EXECUTION_NODE_NB-1:0]                         selected_next_exn_onehot0;
  reg  [$clog2(EXECUTION_NODE_NB)-1:0]                 selected_next_exn;
  reg  [$clog2(EXECUTION_NODE_NB)-1:0]                 selected_next_priority;
  reg  [$clog2(EXECUTION_NODE_NB)-1:0]                 next_ready_exn;

  reg  [EXECUTION_NODE_NB-1:0] grant_global_reg_read;

  // Store EXN number of super blocks
  reg  [$clog2(EXECUTION_NODE_NB)-1:0] exec_table_exn   [EXECUTION_NODE_NB-1:0];
  // Store block index in super block's block descriptor
  reg  [$clog2(MAX_SUBBLOCK_NB)-1:0]   exec_table_index [EXECUTION_NODE_NB-1:0];

  // Unpack signals
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] i_next_block_index [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY($clog2(MAX_SUBBLOCK_NB), EXECUTION_NODE_NB,
                i_next_block_index, i_p_next_block_index, unpack_i0)

  wire [BID_SIZE-1:0] i_next_bid [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY(BID_SIZE, EXECUTION_NODE_NB,
                i_next_bid, i_p_next_bid, unpack_i1)

  wire [EXEC_MODE_SIZE-1:0] i_next_block_exec_mode [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY(EXEC_MODE_SIZE, EXECUTION_NODE_NB,
                i_next_block_exec_mode, i_p_next_block_exec_mode, unpack_i2)

  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] i_branch_block_index [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY($clog2(MAX_SUBBLOCK_NB), EXECUTION_NODE_NB,
                i_branch_block_index, i_p_branch_block_index, unpack_i3)

  wire [$clog2(GLOBAL_REG_NB)-1:0] i_inputs_global_addr [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY($clog2(GLOBAL_REG_NB), EXECUTION_NODE_NB,
                i_inputs_global_addr, i_p_inputs_global_addr, unpack_i4)

  wire [BID_SIZE-1:0] i_current_bid [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY(BID_SIZE, EXECUTION_NODE_NB,
                i_current_bid, i_p_current_bid, unpack_i5)

  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_instruction_to_fetch_offset [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY($clog2(MAX_INPUTS_BODY_BLOCK_LENGTH), EXECUTION_NODE_NB,
                i_instruction_to_fetch_offset, i_p_instruction_to_fetch_offset, unpack_i6)

  wire [ADDRESS_SIZE-1:0] i_mem_data_to_read_address [EXECUTION_NODE_NB-1:0];
  `UNPACK_ARRAY(ADDRESS_SIZE, EXECUTION_NODE_NB,
                i_mem_data_to_read_address, i_p_mem_data_to_read_address, unpack_i7)

  // Pack signals
  reg  [$clog2(EXECUTION_NODE_NB)-1:0] o_exec_priority [EXECUTION_NODE_NB-1:0];
  `PACK_ARRAY($clog2(EXECUTION_NODE_NB), EXECUTION_NODE_NB,
              o_exec_priority, o_p_exec_priority, pack_i0)

  integer k;
  integer j;

  // EXN 0 is to run first block
  initial o_exec_priority[0] = 0;

  assign priority_comparison[0] =  !i_ready[0]
                                && (o_exec_priority[0] < o_exec_priority[1]
                                    || i_ready[1]);

  assign priority_comparison[1] =  !i_ready[0]
                                && (o_exec_priority[0] < o_exec_priority[2]
                                    || i_ready[2]);

  assign priority_comparison[2] =  !i_ready[1]
                                && (o_exec_priority[1] < o_exec_priority[2]
                                    || i_ready[2]);

  always @(*)
  begin
    o_current_exn = 0;

    if (!priority_comparison[0]
      &  priority_comparison[2])
      o_current_exn = 1;

    // Testing non-readiness here is needed because
    // it is not tested in priority_comparison
    if (!priority_comparison[1]
      & !priority_comparison[2]
      & !i_ready[2])
      o_current_exn = 2;
  end

  assign selected_next_exn_onehot0[0] =   priority_comparison[0]
                                      &   priority_comparison[1]
                                      &   priority_comparison[2]
                                      &  (i_next_block_valid[0]
                                       && i_next_block_exec_mode[0]
                                            == `EXEC_MODE_SPEC);

  assign selected_next_exn_onehot0[1] = (!priority_comparison[0]
                                      || (i_next_block_valid[0]
                                       && i_next_block_exec_mode[0]
                                            != `EXEC_MODE_SPEC))
                                      &   priority_comparison[2]
                                      &  (i_next_block_valid[1]
                                       && i_next_block_exec_mode[1]
                                            == `EXEC_MODE_SPEC);

  // FIXME
  // Testing non-readiness here is needed because
  // it is not tested in priority_comparison
  assign selected_next_exn_onehot0[2] = (!priority_comparison[1]
                                      || (i_next_block_valid[0]
                                       && i_next_block_exec_mode[0]
                                            != `EXEC_MODE_SPEC))
                                      &  !priority_comparison[2]
                                      &  (i_next_block_valid[2]
                                       && i_next_block_exec_mode[2]
                                            == `EXEC_MODE_SPEC
                                      &   !i_ready[2]);

  always @(*)
  begin
    selected_next_exn = 0;
    selected_next_priority = 0;

    for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
    begin: gen
      if (selected_next_exn_onehot0 == 2**k)
      begin
        selected_next_exn = k[$clog2(EXECUTION_NODE_NB)-1:0];
        selected_next_priority = o_exec_priority[k] + 1;
      end
    end
  end

  // EXN to be chosen next
  always @(*)
  begin
    next_ready_exn = 0;

    if (!i_ready[0])
      if (i_ready[1])
        next_ready_exn = 1;
      else
        if (i_ready[2])
          next_ready_exn = 2;
  end

  // Scheduling logic
  always @(posedge i_clk)
  begin
    o_selected_next_valid <= {EXECUTION_NODE_NB{1'b0}};
    o_next_block_index_inc <= {EXECUTION_NODE_NB{1'b0}};

    if (i_ready == {EXECUTION_NODE_NB{1'b1}})
    begin
      // Schedule the next block if it has not been possible to speculatively
      // execute it

      for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
      begin: gen1
        o_exec_priority[k] <= 0;
      end

      // FIXME: remove this if/else block (it is only needed until a prefetcher
      // is added)
      // if (i_next_block_valid[0] && i_next_bid[0] == 17*4
      //  || i_next_block_valid[1] && i_next_bid[1] == 17*4)
      // begin
      //   exec_table_exn[1] <= o_current_exn;
      //   exec_table_index[3] <= 0;
      //   o_next_block_index_inc <= 4'b0001;
      //   o_selected_next_bid <= 17*4;
      //   o_selected_next_block_exec_mode <= o_current_exn == 0 ? i_next_block_exec_mode[0] : i_next_block_exec_mode[1];
      //   o_selected_next_valid[1] <= 1;
      // end
      // else
      // begin
      //   if (i_next_block_valid[0] && i_next_bid[0] == 0
      //    || i_next_block_valid[1] && i_next_bid[1] == 0)
      //   begin
      //     exec_table_exn[0] <= o_current_exn;
      //     exec_table_index[0] <= 0;
      //     o_next_block_index_inc <= 4'b0010; // FIXME
      //     o_selected_next_bid <= 0;
      //     o_selected_next_block_exec_mode <= o_current_exn == 0 ? i_next_block_exec_mode[0] : i_next_block_exec_mode[1];
      //     o_selected_next_valid[0] <= 1;
      //   end
      // end

      // FIXME: fix index
      exec_table_exn[o_current_exn] <= o_current_exn;
      // FIXME: fix index and value
      exec_table_index[o_current_exn] <= 0; // Not very useful
      // FIXME: fix value
      o_next_block_index_inc <= {EXECUTION_NODE_NB{1'b0}}; // Not very useful
      // FIXME: fix index?
      o_selected_next_valid[o_current_exn] <= 1;

      for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
      begin: gen2
        if (o_current_exn == k[$clog2(EXECUTION_NODE_NB)-1:0])
        begin
          o_selected_next_bid <= i_next_bid[k];
        end
      end
    end
    else
    begin
      // If there is a block to speculatively execute and an available EXN to
      // execute it
      if (selected_next_exn_onehot0 != {EXECUTION_NODE_NB{1'b0}}
       && i_ready != {EXECUTION_NODE_NB{1'b0}})
      begin
        // Speculatively execute the next block; it only schedules one new block
        // by clock cycle, which is enough because of the time needed to load
        // the next block from memory
        exec_table_exn[next_ready_exn] <= selected_next_exn;
        o_next_block_index_inc <= selected_next_exn_onehot0;
        o_selected_next_valid[next_ready_exn] <= 1;

        for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
        begin: gen3
          if (selected_next_exn == k[$clog2(EXECUTION_NODE_NB)-1:0])
          begin
            exec_table_index[next_ready_exn] <= i_next_block_index[k];
            o_selected_next_bid <= i_next_bid[k];
          end
        end

        for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
        begin: gen4
          if (next_ready_exn == k[$clog2(EXECUTION_NODE_NB)-1:0])
          begin
            o_exec_priority[k] <= selected_next_priority;
          end
        end
      end
    end

    // Update EXN priorities
    if (i_current_block_ended)
    begin
      for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
      begin: gen5
        if (o_exec_priority[k] != 0)
        begin
          o_exec_priority[k] <= o_exec_priority[k] - 1;
        end
      end
    end
  end

  // Discarding logic
  // It is not needed to reset exec_table_exn and exec_table_index because
  // either the EXN is currently executing a block, in this case tables have
  // been updated accordingly, or the EXN is ready and discarding would do
  // nothing
  always @(posedge i_clk)
  begin
    o_discard_exn <= {EXECUTION_NODE_NB{1'b0}};

    for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
    begin: gen6
      if (i_discard_subblocks[k])
      begin
        // Discard the EXNs if and only if they are executing subblocks of the one
        // executing on EXN k
        for (j = 0; j < EXECUTION_NODE_NB; j = j + 1)
        begin: gen
          if (j != k)
          begin
            o_discard_exn[j] <= !i_ready[j]
                             && exec_table_exn[j] == k[$clog2(EXECUTION_NODE_NB)-1:0];
          end
        end
      end
      else
        if (i_next_block_valid[k])
        begin
          // FIXME: what happens if the block running on EXN k is supposed to
          // branch to a block that has not been speculatively executed?

          // Discard the EXN j if and only if it is executing a subblock of the one
          // executing on EXN k and is not going to be branched to
          for (j = 0; j < EXECUTION_NODE_NB; j = j + 1)
          begin: gen6
            if (j != k)
            begin
              o_discard_exn[j] <= !i_ready[j]
                               && exec_table_exn[j] == k[$clog2(EXECUTION_NODE_NB)-1:0]
                               && (!i_branch_block_index_valid[k] // FIXME: is this condition needed?
                                 || exec_table_index[j] != i_branch_block_index[k]);
            end
          end
        end
    end
  end

  // Global register file access handling (READ/WRITE instructions arbitration)
  always @(*)
  begin
    // Default values
    o_global_reg_addr = i_inputs_global_addr[0];
    o_global_reg_write_en = 0;

    if (i_write_fifo_data_available_muxed)
    begin
      // WRITE (highest priority since blocks depend on the completion of the
      // current block, which is the only one allowed to write)
      o_global_reg_write_en = 1;
    end
    else
    begin
      // FIXME: add an else between these conditions?

      // READ (arbitrate reads according to execution node priorities)
      if ((priority_comparison[0] || !i_request_global_reg_read[1])
        & (priority_comparison[1] || !i_request_global_reg_read[2])
        &  i_request_global_reg_read[0])
      begin
      end

      if ((!priority_comparison[0] || !i_request_global_reg_read[0])
        &  (priority_comparison[1] || !i_request_global_reg_read[2])
        &   i_request_global_reg_read[1])
      begin
        o_global_reg_addr = i_inputs_global_addr[1];
      end

      if ((!priority_comparison[1] || !i_request_global_reg_read[0])
        & (!priority_comparison[2] || !i_request_global_reg_read[1])
        &   i_request_global_reg_read[2])
      begin
        o_global_reg_addr = i_inputs_global_addr[2];
      end
    end
  end

  // TODO: refactor this process with the one above
  always @(posedge i_clk)
  begin
    // Delay the grant signal by one clock cycle, waiting for the global register to be read
    o_grant_global_reg_read <= grant_global_reg_read;

    // Default values
    grant_global_reg_read <= 0;

    if (!i_write_fifo_data_available_muxed)
    begin
      // FIXME: add an else between these conditions?

      // READ (arbitrate reads according to execution node priorities)
      if ((priority_comparison[0] || !i_request_global_reg_read[1])
        & (priority_comparison[1] || !i_request_global_reg_read[2])
        &  i_request_global_reg_read[0])
      begin
        grant_global_reg_read[0] <= 1;
      end

      if ((!priority_comparison[0] || !i_request_global_reg_read[0])
        &  (priority_comparison[1] || !i_request_global_reg_read[2])
        &   i_request_global_reg_read[1])
      begin
        grant_global_reg_read[1] <= 1;
      end

      if ((!priority_comparison[1] || !i_request_global_reg_read[0])
        & (!priority_comparison[2] || !i_request_global_reg_read[1])
        &   i_request_global_reg_read[2])
      begin
        grant_global_reg_read[2] <= 1;
      end
    end
  end

  // Memory access handling
  always @(*) // FIXME: check if this can indeed be combinatorial
  begin
    // Default values
    o_mem_bid           = 0;
    o_mem_block_offset  = 0;
    o_mem_address       = 0;
    o_mem_data          = 0;
    o_mem_instr_or_data = 0;
    o_mem_rw            = 0;
    o_mem_valid         = 0;


    if (i_str_fifo_data_available_muxed)
    begin
      o_mem_address = i_str_fifo_output_muxed[ADDRESS_SIZE+WORD_SIZE-1:WORD_SIZE];
      o_mem_data    = i_str_fifo_output_muxed[WORD_SIZE-1:0];
      o_mem_instr_or_data = 1; // Data
      o_mem_rw = 1; // Write
      o_mem_valid = 1;
    end
    else
    begin
      // TODO: maybe refactor this if possible

      if ((i_mem_data_to_read_valid[0] || i_instruction_to_fetch_valid[0])
        & (priority_comparison[0] || !i_instruction_to_fetch_valid[1] || !i_mem_data_to_read_valid[1])
        & (priority_comparison[1] || !i_instruction_to_fetch_valid[2] || !i_mem_data_to_read_valid[2]))
      begin
        if (i_mem_data_to_read_valid[0])
        begin
          `UPDATE_MEM_DATA_TO_READ(0)
        end
        else
        begin
          `UPDATE_MEM_INSTRUCTION_TO_FETCH(0)
        end
      end
      else
      begin
        if ((i_mem_data_to_read_valid[1] || i_instruction_to_fetch_valid[1])
          & (!priority_comparison[0] || !i_instruction_to_fetch_valid[0] || !i_mem_data_to_read_valid[0])
          &  (priority_comparison[2] || !i_instruction_to_fetch_valid[2] || !i_mem_data_to_read_valid[2]))
        begin
          if (i_mem_data_to_read_valid[1])
          begin
            `UPDATE_MEM_DATA_TO_READ(1)
          end
          else
          begin
            `UPDATE_MEM_INSTRUCTION_TO_FETCH(1)
          end
        end
        else
        begin
          if ((i_mem_data_to_read_valid[2] || i_instruction_to_fetch_valid[2])
            & (!priority_comparison[1] || !i_instruction_to_fetch_valid[0] || !i_mem_data_to_read_valid[0])
            & (!priority_comparison[2] || !i_instruction_to_fetch_valid[1] || !i_mem_data_to_read_valid[1]))
          begin
            if (i_mem_data_to_read_valid[2])
            begin
              `UPDATE_MEM_DATA_TO_READ(2)
            end
            else
            begin
              `UPDATE_MEM_INSTRUCTION_TO_FETCH(2)
            end
          end
        end
      end
    end
  end


`ifdef FORMAL
`include "onehot0.v"
  always @(posedge i_clk)
  begin
    // There must be at most one selected EXN at once
    assert(onehot0(selected_next_exn_onehot0));

    // There must be at most one selected EXN at once
    assert(onehot0(o_selected_next_valid));

    // There must be at most one EXN at once whose the block index is
    // incremented
    assert(onehot0(o_next_block_index_inc));

    // There must be at most one EXN at once whose instruction fetch input
    // is marked as valid
    assert(onehot0(o_instruction_fetch_valid));
  end
`endif
endmodule

`endif // __SCHEDULER_MODULE__
