/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __SRAM_CONTROLLER_MODULE__
`define __SRAM_CONTROLLER_MODULE__

`include "isa_const.v"
`include "processor_const.v"

module sram_controller #(
  parameter WORD_SIZE                    = `WORD_SIZE,
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter BID_SIZE                     = `BID_SIZE,
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH,
  parameter RAM_WORD_SIZE                = `RAM_WORD_SIZE,
  parameter RAM_ADDR_SIZE                = `RAM_ADDR_SIZE
)(
  input  wire                                            i_clk, // Can work up to 100 MHz
  input  wire [BID_SIZE-1:0]                             i_bid,
  input  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_word_block_offset,
  input  wire [ADDRESS_SIZE-1:0]                         i_addr,
  input  wire                                            i_input_instr_or_data,
  input  wire [WORD_SIZE-1:0]                            i_data,
  input  wire                                            i_rw, // 0: read, 1: write
  input  wire                                            i_valid,
  inout  wire [RAM_WORD_SIZE-1:0]                        io_sram_data,
  output reg  [RAM_ADDR_SIZE-1:0]                        o_sram_addr = 0,
  output reg                                             o_sram_write_en_n = 1,
  output reg                                             o_sram_output_en_n = 1,
  output wire                                            o_sram_chip_select_n,
  output reg  [BID_SIZE-1:0]                             o_bid                  = 0,
  output reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] o_word_block_offset    = 0,
  output reg  [WORD_SIZE-1:0]                            o_output_word = 0,
  output reg  [ADDRESS_SIZE-1:0]                         o_word_addr = 0,
  output reg                                             o_output_instr_or_data = 0,
  output reg                                             o_output_valid = 0
);

  localparam STATE_NB  = 8;
  localparam IDLE                = 3'd0,
             SET_WRITE_LOWER_HF  = 3'd1,
             SET_WRITE_COMMIT    = 3'd2,
             SET_WRITE_HIGHER_HF = 3'd3,
             SET_READ_LOWER_HF   = 3'd4,
             READ_LOWER_HF       = 3'd5,
             SET_READ_HIGHER_HF  = 3'd6,
             READ_HIGHER_HF      = 3'd7;

  reg [$clog2(STATE_NB)-1:0] state = IDLE;
  reg [$clog2(STATE_NB)-1:0] next_state = IDLE;

  `ifdef VERILATOR
  // reg [RAM_WORD_SIZE-1:0] mock_memory [2**RAM_ADDR_SIZE-1:0];
  reg [RAM_WORD_SIZE-1:0] mock_memory [128-1:0];
  `endif

  reg  [BID_SIZE-1:0]                             current_bid = 0;
  reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] current_word_block_offset = 0;
  reg  [ADDRESS_SIZE-1:0]                         current_addr;
  reg  [WORD_SIZE/2-1:0]                          current_data_upper_hf;
  reg                                             current_instr_or_data;

  reg                      sram_writing = 0;
  wire [RAM_WORD_SIZE-1:0] in_sram_data;
  reg  [RAM_WORD_SIZE-1:0] out_sram_data = 0;

  // TODO: maybe introduce signals for _n
  // TODO: pipeline the controller as much as possible

  always @(*)
  begin
    next_state = state;
    case(state)
      IDLE:
      begin
        if (i_valid)
        begin
          if (i_rw)
          begin
            next_state = SET_WRITE_LOWER_HF;
          end
          else
          begin
            next_state = SET_READ_LOWER_HF;
          end
        end
      end
      SET_WRITE_LOWER_HF:
      begin
        next_state = SET_WRITE_COMMIT;
      end
      // FIXME: is this state really needed?
      SET_WRITE_COMMIT:
      begin
        next_state = SET_WRITE_HIGHER_HF;
      end
      SET_WRITE_HIGHER_HF:
      begin
        next_state = IDLE;
      end
      SET_READ_LOWER_HF:
      begin
        next_state = READ_LOWER_HF;
      end
      READ_LOWER_HF:
      begin
        next_state = SET_READ_HIGHER_HF;
      end
      SET_READ_HIGHER_HF:
      begin
        next_state = READ_HIGHER_HF;
      end
      READ_HIGHER_HF:
      begin
        next_state = IDLE;
      end
      default:
      begin
      end
    endcase
  end

  always @(posedge i_clk)
  begin
    state <= next_state;

    case(next_state)
      IDLE:
      begin
        o_sram_write_en_n <= 1;
        o_sram_output_en_n <= 1;
        o_output_instr_or_data <= 0;
        o_output_valid <= 0;
        sram_writing <= 0;
      end
      SET_WRITE_LOWER_HF:
      begin
        o_sram_addr <= i_addr[RAM_ADDR_SIZE-1:0];
        out_sram_data <= i_data[WORD_SIZE/2-1:0];
        o_sram_write_en_n <= 0;
        o_sram_output_en_n <= 1; // Does not matter during write
        o_output_valid <= 0;
        current_addr <= i_addr;
        current_data_upper_hf <= i_data[WORD_SIZE-1:WORD_SIZE/2];
        sram_writing <= 1;

        `ifdef VERILATOR
        mock_memory[i_addr] <= i_data[WORD_SIZE/2-1:0];
        `endif
      end
      SET_WRITE_COMMIT:
      begin
        o_sram_write_en_n <= 1;
        sram_writing <= 1;
      end
      SET_WRITE_HIGHER_HF:
      begin
        o_sram_addr <= current_addr[RAM_ADDR_SIZE-1:0] + 1;
        out_sram_data <= current_data_upper_hf;
        o_sram_write_en_n <= 0;
        o_sram_output_en_n <= 1; // Does not matter during write
        sram_writing <= 1;

        `ifdef VERILATOR
        mock_memory[current_addr+1] <= current_data_upper_hf;
        `endif
      end
      SET_READ_LOWER_HF:
      begin
        o_sram_addr <= i_addr[RAM_ADDR_SIZE-1:0];
        o_sram_write_en_n <= 1;
        o_sram_output_en_n <= 0;
        o_output_valid <= 0;
        current_bid <= i_bid;
        current_word_block_offset <= i_word_block_offset;
        current_addr <= i_addr;
        current_instr_or_data <= i_input_instr_or_data;
        sram_writing <= 0;
      end
      READ_LOWER_HF:
      begin
        o_output_word[WORD_SIZE/2-1:0] <= in_sram_data;
        sram_writing <= 0;

        `ifdef VERILATOR
        o_output_word[WORD_SIZE/2-1:0] <= mock_memory[current_addr];
        `endif
      end
      SET_READ_HIGHER_HF:
      begin
        o_sram_addr <= i_addr[RAM_ADDR_SIZE-1:0] + 1;
        o_sram_write_en_n <= 1;
        o_sram_output_en_n <= 0;
        sram_writing <= 0;
      end
      READ_HIGHER_HF:
      begin
        o_output_word[WORD_SIZE-1:WORD_SIZE/2] <= in_sram_data;
        o_output_valid <= 1;
        o_bid <= current_bid;
        o_word_block_offset <= current_word_block_offset;
        o_word_addr <= current_addr;
        o_output_instr_or_data <= current_instr_or_data;
        sram_writing <= 0;

        `ifdef VERILATOR
        o_output_word[WORD_SIZE-1:WORD_SIZE/2] <= mock_memory[current_addr+1];
        `endif
      end
      default:
      begin
        o_sram_write_en_n <= 1;
        o_sram_output_en_n <= 1;
        sram_writing <= 0;
        o_output_valid <= 0;
      end
    endcase
  end

  assign io_sram_data = sram_writing ? out_sram_data : {RAM_WORD_SIZE{1'bZ}};
  assign in_sram_data = io_sram_data;

  // TODO: select it more wisely to reduce energy consumption
  assign o_sram_chip_select_n = 0; // Always select the SRAM
endmodule

`endif // __SRAM_CONTROLLER_MODULE__
