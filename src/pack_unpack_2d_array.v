/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`ifndef PACK_ARRAY
`define PACK_ARRAY(PK_WIDTH,PK_LEN,PK_SRC,PK_DEST,PK_IDX) \
genvar PK_IDX; \
generate \
  for (PK_IDX = 0; PK_IDX < (PK_LEN); PK_IDX = PK_IDX + 1) begin \
    assign PK_DEST[(PK_WIDTH)*PK_IDX +: (PK_WIDTH)] = PK_SRC[PK_IDX][((PK_WIDTH)-1):0]; \
  end \
endgenerate
`endif

`ifndef UNPACK_ARRAY
`define UNPACK_ARRAY(PK_WIDTH,PK_LEN,PK_DEST,PK_SRC,UNPK_IDX) \
genvar UNPK_IDX; \
generate \
  for (UNPK_IDX = 0; UNPK_IDX < (PK_LEN); UNPK_IDX = UNPK_IDX + 1) begin \
    assign PK_DEST[UNPK_IDX][((PK_WIDTH)-1):0] = PK_SRC[(PK_WIDTH)*UNPK_IDX +: (PK_WIDTH)]; \
  end \
endgenerate
`endif
