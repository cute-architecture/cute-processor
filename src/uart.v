/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __UART_MODULE__
`define __UART_MODULE__

`include "to_array_max.v"
`include "processor_const.v"

module uart #(
  parameter CLK_DIV     = `EXTERN_CLK_FREQ / `GLOBAL_CLK_DIV / `UART_BITRATE,
  parameter WORD_SIZE   = 8,
  parameter PARITY_BIT  = 0, // 0 is for no parity bit, 1 for odd parity and 2 for even parity
  parameter STOP_LENGTH = 1 // 1 or 2 bits (1.5 is not yet supported)
)(
  input  wire                 i_clk,
  input  wire [WORD_SIZE-1:0] i_input_tx,
  input  wire                 i_input_valid,
  output wire                 o_uart_clk,
  output reg                  o_uart_tx = 1,
  output reg                  o_is_ready = 1
);

  localparam STATE_NB = 5;
  localparam IDLE     = 3'd0,
             START    = 3'd1,
             DATA_TX  = 3'd2,
             PARITY   = 3'd3,
             STOP     = 3'd4;

  reg [$clog2(STATE_NB)-1:0]  state = IDLE;
  reg [$clog2(WORD_SIZE)-1:0] bit_cnt;
  reg [WORD_SIZE-1:0]         data_tx;

  // Clock generation
  reg  [$clog2(CLK_DIV-1)-1:0] q;

  // TODO: improve this frequency division (use a PLL?)
  // FIXME: improve this clock divider
  always @(posedge i_clk)
    if (q == CLK_DIV[$clog2(CLK_DIV-1)-1:0] - 1)
      q <= 0;
    else
      q <= q + 1;

  assign o_uart_clk = q[$clog2(CLK_DIV-1)-1] == 1;

  always @(posedge o_uart_clk)
    case(state)
      IDLE:
      begin
        o_uart_tx <= 1;
        o_is_ready <= 1;

        if (i_input_valid)
        begin
          o_is_ready <= 0;

          state <= START;
        end
        else
          state <= state;
      end
      START:
      begin
        o_uart_tx <= 0;
        bit_cnt <= 0;
        o_is_ready <= 0;

        data_tx <= i_input_tx; // Latch the input word

        state <= DATA_TX;
      end
      DATA_TX:
      begin
        o_uart_tx <= data_tx[bit_cnt]; // Write output bit value
        bit_cnt <= bit_cnt + 1;

        if (bit_cnt == `TO_ARRAY_MAX(WORD_SIZE))
          if (PARITY_BIT)
            state <= PARITY;
          else
            state <= STOP;
        else
          state <= state;
        end
      PARITY:
      begin
        if (PARITY_BIT == 1)
          o_uart_tx <= !^data_tx; // Xor bits for even parity
        else
          o_uart_tx <= ^data_tx; // Xor bits for odd parity

        state <= STOP;
      end
      STOP:
      begin
        o_uart_tx <= 1;
        o_is_ready <= 1;

        if (i_input_valid)
          state <= START;
        else
          state <= IDLE;
      end
      default:
      begin
        state <= IDLE;
      end
    endcase

endmodule

`endif // __UART_MODULE__
