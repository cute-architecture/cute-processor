/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __GPIO_CONTROLLER_MODULE__
`define __GPIO_CONTROLLER_MODULE__

`include "isa_const.v"
`include "processor_const.v"

module gpio_controller #(
  parameter BYTE_SIZE = `BYTE_SIZE,
  parameter GPIO_NB   = `GPIO_NB
)(
  input  wire                 i_clk,
  /* verilator lint_off UNUSED */
  input  wire [BYTE_SIZE-1:0] i_data,
  /* verilator lint_on UNUSED */
  input  wire                 i_input_valid,
  output reg  [GPIO_NB-1:0]   o_gpio
);

  always @(posedge i_clk)
    if (i_input_valid)
    begin
      o_gpio <= i_data[GPIO_NB-1:0];
    end

endmodule

`endif // __GPIO_CONTROLLER_MODULE__
