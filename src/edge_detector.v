/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EDGE_DETECTOR_MODULE__
`define __EDGE_DETECTOR_MODULE__

// FIXME: write a testbench

module edge_detector #(
  parameter EDGE_TYPE = 1 // 1 for rising edge, 0 for falling edge
)(
  input  wire i_clk,
  input  wire i_signal,
  output reg  o_edge_triggered = 0
);

  reg  flag = 0;

  always @(posedge i_clk)
    if (i_signal == EDGE_TYPE && !flag)
    begin
      flag <= 1;
      o_edge_triggered <= 1;
    end
    else
      if (i_signal != EDGE_TYPE && flag)
      begin
        flag <= 0;
        o_edge_triggered <= 0;
      end
      else
        o_edge_triggered <= 0;

endmodule

`endif // __EDGE_DETECTOR_MODULE__

