/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EXEC_NODE_DECODE_MODULE__
`define __EXEC_NODE_DECODE_MODULE__

`include "isa_const.v"

module exec_node_decode #(
  parameter INSTRUCTION_SIZE     = `INSTRUCTION_SIZE,
  parameter OPCODE_SIZE          = `OPCODE_SIZE,
  parameter CONDITION_SIZE       = `CONDITION_SIZE,
  parameter CONDITION_JUMP_SIZE  = `CONDITION_JUMP_SIZE,
  parameter IMMEDIATE_SIZE       = `IMMEDIATE_SIZE,
  parameter SHORT_IMMEDIATE_SIZE = `SHORT_IMMEDIATE_SIZE,
  parameter GLOBAL_REG_NB        = `GLOBAL_REG_NB,
  parameter LOCAL_REG_NB         = `LOCAL_REG_NB,
  parameter MAX_SUBBLOCK_NB      = `MAX_SUBBLOCK_NB
)(
  input  wire                               i_clk,
  input  wire                               i_reset,
  input  wire                               i_en,
  input  wire [INSTRUCTION_SIZE-1:0]        i_instruction,
  output reg  [OPCODE_SIZE-1:0]             o_opcode,
  output reg                                o_imm_sel,
  output reg                                o_imm_upper_half, // TODO: maybe rename this
  output reg  [IMMEDIATE_SIZE-1:0]          o_imm_value,
  output reg  [$clog2(LOCAL_REG_NB)-1:0]    o_addr_a,
  output reg  [$clog2(LOCAL_REG_NB)-1:0]    o_addr_b,
  output reg  [$clog2(LOCAL_REG_NB)-1:0]    o_addr_d,
  output reg  [CONDITION_SIZE-1:0]          o_condition,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0] o_branch_block_index_t,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0] o_branch_block_index_f,
  output reg  [$clog2(GLOBAL_REG_NB)-1:0]   o_addr_w,
  output reg                                o_reg_write_en = 0,
  output reg                                o_write_fifo_in_en = 0,
  output reg                                o_str_fifo_in_en = 0
);

  // FIXME: watch out for the null instruction (should only appear when the
  // instruction cache is not full)

  always @(posedge i_clk)
  begin
    if (i_reset)
    begin
      o_opcode <= 0;
      o_reg_write_en <= 0;
      o_write_fifo_in_en <= 0;
      o_str_fifo_in_en <= 0;
    end
    else
      if (i_en)
      begin
        o_opcode <= i_instruction[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-OPCODE_SIZE];

        // The opcode needs to be repeated because it is not yet latched
        case(i_instruction[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-OPCODE_SIZE])
          `ADD_OP, `SUB_OP, `XOR_OP, `AND_OP, `OR_OP:
          begin
            o_imm_sel <= i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1];
            o_reg_write_en <= 1;
            o_write_fifo_in_en <= 0;
            o_str_fifo_in_en <= 0;

            o_addr_b <= i_instruction[$clog2(LOCAL_REG_NB)-1:0];
            o_addr_d <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                          $clog2(LOCAL_REG_NB)];

            // The imm_sel needs to be repeated because it is not yet latched
            if (i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1])
            begin
              o_imm_upper_half <= i_instruction[IMMEDIATE_SIZE+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
              o_imm_value <= i_instruction[IMMEDIATE_SIZE+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                          $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
            end
            else
              o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                            $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
          end
          `CMP_OP:
          begin
            o_imm_sel <= i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1];
            o_reg_write_en <= 1;
            o_write_fifo_in_en <= 0;
            o_str_fifo_in_en <= 0;

            o_addr_b <= i_instruction[$clog2(LOCAL_REG_NB)-1:0];
            o_addr_d <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                      $clog2(LOCAL_REG_NB)];
            // o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
            //                           $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];

            o_condition <= i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1-1:
                                        INSTRUCTION_SIZE-OPCODE_SIZE-1-CONDITION_SIZE];

            // The imm_sel needs to be repeated because it is not yet latched
            if (i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1])
            begin
              o_imm_upper_half <= 0; // TODO: add a bit in the instruction too
              // TODO: try to adapt the ISA to be able to factor out these lines
              o_imm_value <= {
                {IMMEDIATE_SIZE-SHORT_IMMEDIATE_SIZE{1'b0}},
                 i_instruction[SHORT_IMMEDIATE_SIZE+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                    $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)]
              };
            end
            else
            begin
              o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                             $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
            end
          end
          `BR_OP:
          begin
            o_imm_sel <= 0;
            o_reg_write_en <= 0;
            o_write_fifo_in_en <= 0;
            o_str_fifo_in_en <= 0;

            o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)-1:0];

            o_branch_block_index_f <= i_instruction[$clog2(MAX_SUBBLOCK_NB)+$clog2(LOCAL_REG_NB)-1:
                                                    $clog2(LOCAL_REG_NB)];
            o_branch_block_index_t <= i_instruction[$clog2(MAX_SUBBLOCK_NB)+$clog2(MAX_SUBBLOCK_NB)+$clog2(LOCAL_REG_NB)-1:
                                                    $clog2(MAX_SUBBLOCK_NB)+$clog2(LOCAL_REG_NB)];
          end
          `WRITE_OP:
          begin
            o_imm_sel <= i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1];
            o_reg_write_en <= 0;
            o_write_fifo_in_en <= 1;
            o_str_fifo_in_en <= 0;

            o_addr_w <= i_instruction[$clog2(GLOBAL_REG_NB)-1:0];

            if (i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1])
            begin
              o_imm_upper_half <= i_instruction[IMMEDIATE_SIZE+$clog2(GLOBAL_REG_NB)];
              o_imm_value <= i_instruction[IMMEDIATE_SIZE+$clog2(GLOBAL_REG_NB)-1:
                                          $clog2(GLOBAL_REG_NB)];
            end
            else
            begin
              // TODO: try to adapt the ISA to be able to factor out these lines
              o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(GLOBAL_REG_NB)-1:
                                        $clog2(GLOBAL_REG_NB)];
            end
          end
          `STR_OP:
          begin
            o_imm_sel <= i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1];
            o_reg_write_en <= 0;
            o_write_fifo_in_en <= 0;
            o_str_fifo_in_en <= 1;

            // TODO: maybe factor out these lines
            o_addr_b <= i_instruction[$clog2(LOCAL_REG_NB)-1:0];

            // TODO: maybe factor out these lines
            // The imm_sel needs to be repeated because it is not yet latched
            if (i_instruction[INSTRUCTION_SIZE-OPCODE_SIZE-1])
            begin
              o_imm_upper_half <= i_instruction[IMMEDIATE_SIZE+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
              o_imm_value <= i_instruction[IMMEDIATE_SIZE+$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                          $clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)];
            end
            else
              o_addr_a <= i_instruction[$clog2(LOCAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:
                                                            $clog2(LOCAL_REG_NB)];
          end
          default:
          begin
            o_reg_write_en <= 0;
            o_write_fifo_in_en <= 0;
            o_str_fifo_in_en <= 0;
          end
        endcase
      end
  end

endmodule

`endif // __EXEC_NODE_DECODE_MODULE__
