/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __FLASH_READ_CONTROLLER_MODULE__
`define __FLASH_READ_CONTROLLER_MODULE__

`include "isa_const.v"
`include "processor_const.v"

`define LATCH_INPUTS \
current_bid <= i_bid; \
current_word_block_offset <= i_word_block_offset; \
current_word_address <= i_word_address; \
current_instr_or_data <= i_input_instr_or_data; \

// TODO: support dual read
module flash_read_controller #(
  `ifdef VERILATOR
  parameter MOCK_MEMORY_LENGTH = 2**SPI_ADDRESS_SIZE,
  parameter MOCK_MEMORY_FILE   = "",
  `endif
  // parameter CLK_RATIO                    = 2, // /!\ SPI clock is f(i_clk) / CLK_RATIO / 2
  parameter FLASH_BYTE_SIZE              = `FLASH_BYTE_SIZE,
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter SPI_ADDRESS_SIZE             = `SPI_ADDRESS_SIZE,
  parameter WORD_SIZE                    = `WORD_SIZE,
  parameter FAST_READ_EN                 = 0, // FIXME: data read does not seem accurate
  parameter CPOL                         = 1, // This must be either 0 or 1 // TODO: remove this configuration?
  parameter BID_SIZE                     = `BID_SIZE,
  parameter EXECUTION_NODE_NB            = `EXECUTION_NODE_NB,
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH
)(
  input  wire                                            i_clk,
  input  wire [BID_SIZE-1:0]                             i_bid,
  input  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_word_block_offset,
  input  wire [ADDRESS_SIZE-1:0]                         i_word_address,
  input  wire                                            i_input_instr_or_data,
  input  wire                                            i_input_valid,
  input  wire                                            i_spi_miso,

  output reg                                             o_spi_clk              = CPOL,
  output reg                                             o_spi_mosi             = 0,
  output reg                                             o_spi_ss               = 1,
  output reg  [BID_SIZE-1:0]                             o_bid                  = 0,
  output reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] o_word_block_offset    = 0,
  output reg  [ADDRESS_SIZE-1:0]                         o_word_address         = 0,
  output reg  [WORD_SIZE-1:0]                            o_word_output          = 0,
  output reg                                             o_output_instr_or_data = 0,
  output reg                                             o_output_valid         = 0
);

  localparam STATE_NB = 10;
  localparam IDLE                      = 4'd0,
             WAKE_UP_SELECT            = 4'd1,
             WRITE_WAKE_UP_INSTRUCTION = 4'd2,
             WAIT_WAKE_UP              = 4'd3,
             READ_SELECT               = 4'd4,
             WRITE_READ_INSTRUCTION    = 4'd5,
             WRITE_ADDRESS             = 4'd6,
             DUMMY_CLOCKS              = 4'd7,
             READ_DATA                 = 4'd8,
             END                       = 4'd9;

  localparam FLASH_INSTRUCTION_SIZE = 8;
  localparam READ_INSTRUCTION       = 8'h03;
  localparam FAST_READ_INSTRUCTION  = 8'h0b;
  localparam WAKE_UP_INSTRUCTION    = 8'hab;

  localparam DUMMY_CLK_NB = 8;
  localparam INTER_IDLE_DELAY = 100; // FIXME: compute this from CLK_RATIO (100 for CLK_RATIO == 2)

  // TODO: check vector lengths

  reg  [$clog2(STATE_NB)-1:0] state = IDLE;

  reg  [BID_SIZE-1:0]                             current_bid = 0;
  reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] current_word_block_offset = 0;
  reg  [ADDRESS_SIZE-1:0]                         current_word_address = 0;
  reg                                             current_instr_or_data = 0;
  `ifdef VERILATOR
  /* verilator lint_off UNUSED */
  reg  [WORD_SIZE-1:0]                            current_word = 0;
  /* verilator lint_on UNUSED */
  `else
  reg  [WORD_SIZE-1:0]                            current_word = 0;
  `endif

  `ifdef VERILATOR
  reg  [FLASH_BYTE_SIZE-1:0] mock_memory [MOCK_MEMORY_LENGTH-1:0];
  initial $readmemh(MOCK_MEMORY_FILE, mock_memory);
  `endif

  // 0: powered down, 1: powered up
  // The Flash chip is initially powered down by the FPGA after having read the
  // bitstream
  reg  flash_power_state = 0;

  // TODO: maybe merge these counters into a single one
  reg  [$clog2(FLASH_INSTRUCTION_SIZE):0] wake_up_instruction_cnt;
  reg  [$clog2(INTER_IDLE_DELAY):0]       inter_idle_cnt;
  reg  [$clog2(FLASH_INSTRUCTION_SIZE):0] read_instruction_cnt;
  reg  [$clog2(SPI_ADDRESS_SIZE):0]       address_cnt;
  reg  [$clog2(DUMMY_CLK_NB):0]           dummy_byte_cnt;
  reg  [$clog2(WORD_SIZE):0]              data_bit_cnt;

  // Clock generation
  // wire                           clk;
  // reg  [$clog2(CLK_RATIO-1)+1:0] q;

  // TODO: improve this frequency division (use a PLL?)
  // always @(posedge i_clk)
  //   if (q == CLK_RATIO - 1)
  //     q <= 0;
  //   else
  //     q <= q + 1;

  // assign clk = q == CLK_RATIO - 1;

  // TODO: rewrite this FSM with two processes
  always @(posedge i_clk)
    case(state)
      IDLE:
      begin
        o_spi_clk <= CPOL;
        o_spi_ss <= 1;
        o_output_valid <= 0;

        if (i_input_valid)
        begin
          if (flash_power_state)
          begin
            state <= READ_SELECT;
          end
          else
          begin
            // Wake up the Flash chip only the first time since it has been
            // powered down by the FPGA after loading the bitstream
            state <= WAKE_UP_SELECT;
          end
        end
      end
      WAKE_UP_SELECT:
      begin
        o_spi_ss <= 0;

        wake_up_instruction_cnt <= 0;
        inter_idle_cnt <= 0;

        `LATCH_INPUTS

        state <= WRITE_WAKE_UP_INSTRUCTION;
      end
      WRITE_WAKE_UP_INSTRUCTION:
      begin
        o_spi_clk <= !o_spi_clk;

        if (o_spi_clk == CPOL)
        begin
          o_spi_mosi <= WAKE_UP_INSTRUCTION[FLASH_INSTRUCTION_SIZE - 1 - wake_up_instruction_cnt];
          wake_up_instruction_cnt <= wake_up_instruction_cnt + 1;
        end

        if (wake_up_instruction_cnt == FLASH_INSTRUCTION_SIZE)
        begin
          state <= WAIT_WAKE_UP;
        end
      end
      WAIT_WAKE_UP:
      begin
        o_spi_clk <= CPOL;
        o_spi_mosi <= 0;
        o_spi_ss <= 1;

        inter_idle_cnt <= inter_idle_cnt + 1;

        if (inter_idle_cnt == INTER_IDLE_DELAY)
        begin
          flash_power_state <= 1;
          state <= READ_SELECT;
        end
      end
      READ_SELECT:
      begin
        o_spi_clk <= CPOL;
        o_spi_ss <= 0;

        read_instruction_cnt <= 0;
        address_cnt <= 0;
        data_bit_cnt <= 0;

        `LATCH_INPUTS

        state <= WRITE_READ_INSTRUCTION;
      end
      WRITE_READ_INSTRUCTION:
      begin
        o_spi_clk <= !o_spi_clk;

        if (o_spi_clk == CPOL)
        begin
          if (FAST_READ_EN)
            o_spi_mosi <= FAST_READ_INSTRUCTION[FLASH_INSTRUCTION_SIZE - 1 - read_instruction_cnt];
          else
            o_spi_mosi <= READ_INSTRUCTION[FLASH_INSTRUCTION_SIZE - 1 - read_instruction_cnt];
          read_instruction_cnt <= read_instruction_cnt + 1;
        end

        if (read_instruction_cnt == FLASH_INSTRUCTION_SIZE)
        begin
          state <= WRITE_ADDRESS;
        end
      end
      WRITE_ADDRESS:
      begin
        o_spi_clk <= !o_spi_clk;

        if (o_spi_clk == CPOL)
        begin
          o_spi_mosi <= current_word_address[SPI_ADDRESS_SIZE - 1 - address_cnt];
          address_cnt <= address_cnt + 1;
        end

        if (address_cnt == SPI_ADDRESS_SIZE)
          if (FAST_READ_EN)
            state <= DUMMY_CLOCKS;
          else
            state <= READ_DATA;
      end
      DUMMY_CLOCKS:
      begin
        o_spi_clk <= !o_spi_clk;

        dummy_byte_cnt <= dummy_byte_cnt + 1;

        if (dummy_byte_cnt == DUMMY_CLK_NB)
        begin
          state <= READ_DATA;
        end
      end
      READ_DATA:
      begin
        o_spi_clk <= !o_spi_clk;

        o_spi_mosi <= 0;

        // Read and store input bit value
        if (o_spi_clk == !CPOL) // TODO: change this silly test
        begin
          current_word[WORD_SIZE - 1 - data_bit_cnt] <= i_spi_miso;
          data_bit_cnt <= data_bit_cnt + 1;

          if (data_bit_cnt == WORD_SIZE)
          begin
            o_bid <= current_bid;
            o_word_block_offset <= current_word_block_offset;
            o_word_address <= current_word_address;
            o_output_instr_or_data <= current_instr_or_data;
            `ifdef VERILATOR
            // TODO: this should use WORD_SIZE
            o_word_output <= {mock_memory[current_word_address+0],
                              mock_memory[current_word_address+1],
                              mock_memory[current_word_address+2],
                              mock_memory[current_word_address+3]};
            `else
            o_word_output <= current_word;
            `endif

            state <= END;
          end
          else
          begin
            o_output_valid <= 0;
          end
        end
      end
      END: // FIXME: this should happen earlier
      begin
        o_spi_clk <= CPOL;
        o_spi_ss <= 1;
        o_spi_mosi <= 0;
        o_output_valid <= 1;

        state <= IDLE;
      end
      default:
      begin
        state <= IDLE;
      end
    endcase

`ifdef FORMAL
  always @(posedge i_clk)
  begin
  end
`endif
endmodule

`endif // __FLASH_READ_CONTROLLER_MODULE__
