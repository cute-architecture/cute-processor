/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __TOP_MODULE__
`define __TOP_MODULE__

`include "processor.v"

module top #(
  parameter GPIO_NB = 2,
  parameter RAM_WORD_SIZE = `RAM_WORD_SIZE,
  parameter RAM_ADDR_SIZE = `RAM_ADDR_SIZE
)(
  input  wire                     i_sysclk,
  input  wire                     i_spi_miso,
  output wire                     o_spi_clk,
  output wire                     o_spi_mosi,
  output wire                     o_spi_ss,
  inout  wire [RAM_WORD_SIZE-1:0] io_sram_data,
  output wire [RAM_ADDR_SIZE-1:0] o_sram_addr,
  output wire                     o_sram_write_en_n,
  output wire                     o_sram_output_en_n,
  output wire                     o_sram_chip_select_n,
  output wire [GPIO_NB-1:0]       o_gpio,
  output wire [6-1:0]             o_scope,
  output wire                     o_uart_tx
);

  processor processor (
    .i_clk(i_sysclk),
    .i_spi_miso(i_spi_miso),
    .o_spi_clk(o_spi_clk),
    .o_spi_mosi(o_spi_mosi),
    .o_spi_ss(o_spi_ss),
    .io_sram_data(io_sram_data),
    .o_sram_addr(o_sram_addr),
    .o_sram_write_en_n(o_sram_write_en_n),
    .o_sram_output_en_n(o_sram_output_en_n),
    .o_sram_chip_select_n(o_sram_chip_select_n),
    .o_gpio(o_gpio),
    .o_scope(o_scope),
    .o_uart_tx(o_uart_tx)
  );

endmodule

`endif // __TOP_MODULE__
