/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __COUNTONES_FUNCTION__
`define __COUNTONES_FUNCTION__

`include "processor_const.v"

function integer countones (input [`EXECUTION_NODE_NB-1:0] a);
  integer n;
  begin
    countones = 0;
    for (n = 0; n < `EXECUTION_NODE_NB; n = n + 1)
    begin
      countones = countones + a[n];
    end
  end
endfunction

`endif // __COUNTONES_FUNCTION__
