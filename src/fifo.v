/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

// This FIFO does not handle overflow anyhow

`ifndef __FIFO_MODULE__
`define __FIFO_MODULE__

module fifo #(
  // Choosing these parameters wisely allows the memory to be inferred to BRAM
  parameter DATA_WIDTH  = 16,
  parameter FIFO_LENGTH = 8
)(
  input  wire                  i_clk,
  input  wire                  i_reset,
  input  wire [DATA_WIDTH-1:0] i_data_in,
  input  wire                  i_en_in,
  input  wire                  i_en_out,
  output reg  [DATA_WIDTH-1:0] o_data_out,
  output reg                   o_data_available = 0,
  output reg                   o_last_item = 0
);

  reg  [DATA_WIDTH-1:0]          memory [FIFO_LENGTH-1:0];
  reg  [$clog2(FIFO_LENGTH)-1:0] write_pointer = 0; // FIXME: maybe add an extra bit to avoid overflow?
  reg  [$clog2(FIFO_LENGTH)-1:0] read_pointer  = 0; // FIXME: maybe add an extra bit to avoid overflow?

  always @(posedge i_clk)
  begin
    o_data_out <= memory[read_pointer];

    if (i_reset)
    begin
      // TODO: maybe reset the memory
      read_pointer <= 0;
      write_pointer <= 0;
      o_data_available <= 0;
    end
    else
    begin
      if (i_en_in)
      begin
        memory[write_pointer] <= i_data_in;
        write_pointer <= write_pointer + 1;
        o_data_available <= write_pointer + 1 > read_pointer;
        o_last_item <= write_pointer == read_pointer;
      end
      else
      begin
        if (i_en_out)
        begin
          // This FIFO is not protected against reading with no prior writing:
          // it will output and increment the read pointer even if no data has yet
          // be written
          read_pointer <= read_pointer + 1;
          o_data_available <= write_pointer > read_pointer + 1;
          o_last_item <= write_pointer == read_pointer;
        end
      end
    end
  end

`ifdef FORMAL
  always @(posedge i_clk)
  begin
    // assert(read_pointer <= write_pointer);
  end
`endif
endmodule

`endif // __FIFO_MODULE__
