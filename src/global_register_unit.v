/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __GLOBAL_REGISTER_UNIT_MODULE__
`define __GLOBAL_REGISTER_UNIT_MODULE__

module global_register_unit #(
  parameter WORD_SIZE     = 32,
  parameter GLOBAL_REG_NB = 128
)(
  input  wire                                       i_clk,
  input  wire                                       i_write_en,
  input  wire [$clog2(GLOBAL_REG_NB)-1:0]           i_reg_read_addr,
  input  wire [$clog2(GLOBAL_REG_NB)+WORD_SIZE-1:0] i_write_fifo_output,
  output reg  [WORD_SIZE-1:0]                       o_reg_output_data
);

  reg  [WORD_SIZE-1:0] memory [GLOBAL_REG_NB-1:0];

  always @(posedge i_clk)
  begin
    o_reg_output_data <= memory[i_reg_read_addr];

    if (i_write_en)
      memory[i_write_fifo_output[$clog2(GLOBAL_REG_NB)+WORD_SIZE-1:WORD_SIZE]] <= i_write_fifo_output[WORD_SIZE-1:0];
  end

endmodule

`endif // __GLOBAL_REGISTER_UNIT_MODULE__
