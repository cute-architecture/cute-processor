/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EXEC_NODE_READ_REG_MODULE__
`define __EXEC_NODE_READ_REG_MODULE__

`include "isa_const.v"

module exec_node_read_reg #(
  parameter GLOBAL_REG_NB       = `GLOBAL_REG_NB,
  parameter LOCAL_REG_NB        = `LOCAL_REG_NB,
  parameter IMMEDIATE_SIZE      = `IMMEDIATE_SIZE,
  parameter OPCODE_SIZE         = `OPCODE_SIZE,
  parameter CONDITION_SIZE      = `CONDITION_SIZE,
  parameter MAX_SUBBLOCK_NB     = `MAX_SUBBLOCK_NB
)(
  input  wire                               i_clk,
  input  wire [OPCODE_SIZE-1:0]             i_opcode,
  input  wire                               i_imm_sel,
  input  wire                               i_imm_upper_half,
  input  wire [IMMEDIATE_SIZE-1:0]          i_imm_value,
  input  wire [CONDITION_SIZE-1:0]          i_condition,
  input  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] i_branch_block_index_f,
  input  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] i_branch_block_index_t,
  input  wire [$clog2(LOCAL_REG_NB)-1:0]    i_addr_d,
  input  wire                               i_write_en,
  input  wire [$clog2(GLOBAL_REG_NB)-1:0]   i_addr_w,
  input  wire                               i_write_fifo_in_en,
  input  wire                               i_str_fifo_in_en,
  output reg  [OPCODE_SIZE-1:0]             o_opcode,
  output reg                                o_imm_sel,
  output reg                                o_imm_upper_half,
  output reg  [IMMEDIATE_SIZE-1:0]          o_imm_value,
  output reg  [$clog2(LOCAL_REG_NB)-1:0]    o_addr_d,
  output reg  [CONDITION_SIZE-1:0]          o_condition,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0] o_branch_block_index_f,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0] o_branch_block_index_t,
  output reg                                o_write_en,
  output reg  [$clog2(GLOBAL_REG_NB)-1:0]   o_addr_w,
  output reg                                o_write_fifo_in_en,
  output reg                                o_str_fifo_in_en
);

  always @(posedge i_clk)
  begin
    // ALU signals
    o_opcode <= i_opcode;
    o_imm_sel <= i_imm_sel;
    o_imm_upper_half <= i_imm_upper_half;
    o_imm_value <= i_imm_value;
    o_condition <= i_condition;
    o_branch_block_index_f <= i_branch_block_index_f;
    o_branch_block_index_t <= i_branch_block_index_t;

    // Register write signals
    o_addr_d <= i_addr_d;
    o_write_en <= i_write_en;

    // FIFO signals
    o_addr_w <= i_addr_w;
    o_write_fifo_in_en <= i_write_fifo_in_en;
    o_str_fifo_in_en <= i_str_fifo_in_en;
  end

endmodule

`endif // __EXEC_NODE_EXECUTE_MODULE__
