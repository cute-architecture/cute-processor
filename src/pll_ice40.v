/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * PLL configuration
 *
 * This Verilog module was generated automatically
 * using the (modified) icepll tool from the IceStorm project.
 * Use at your own risk.
 *
 * Given input frequency:       100.000 MHz
 * Requested output frequency:   25.000 MHz
 * Achieved output frequency:    25.000 MHz
 */

`default_nettype none

`ifndef __PLL_ICE40_MODULE__
`define __PLL_ICE40_MODULE__

module pll_ice40 (
  input  i_clock_in,
  output o_global_clock,
  output o_locked
);

  SB_PLL40_CORE #(
    .FEEDBACK_PATH("SIMPLE"),
    .DIVR(4'b0000),       // DIVR =  0
    .DIVF(7'b0000111),    // DIVF =  7
    .DIVQ(3'b101),        // DIVQ =  5
    .FILTER_RANGE(3'b101) // FILTER_RANGE = 5
  ) uut (
    .LOCK(o_locked),
    .RESETB(1'b1),
    .BYPASS(1'b0),
    .REFERENCECLK(i_clock_in),
    .PLLOUTCORE(o_global_clock)
  );

endmodule

`endif // __PLL_ICE40_MODULE__
