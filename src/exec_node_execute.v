/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EXEC_NODE_EXECUTE_MODULE__
`define __EXEC_NODE_EXECUTE_MODULE__

module exec_node_execute #(
  parameter REGISTER_NB         = 16,
  parameter IMMEDIATE_SIZE      = 16,
  parameter OPCODE_SIZE         = 5,
  parameter CONDITION_JUMP_SIZE = 3
)(
  input  wire                           i_clk,
  input  wire [$clog2(REGISTER_NB)-1:0] i_addr_d,
  input  wire                           i_write_en,
  output reg  [$clog2(REGISTER_NB)-1:0] o_addr_d,
  output reg                            o_write_en
);

  always @(posedge i_clk)
  begin
    // Register write signals
    o_addr_d <= i_addr_d;
    o_write_en <= i_write_en;
  end

endmodule

`endif // __EXEC_NODE_EXECUTE_MODULE__
