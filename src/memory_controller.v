/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __MEMORY_CONTROLLER_MODULE__
`define __MEMORY_CONTROLLER_MODULE__

`include "isa_const.v"

// TODO: maybe rename this
module memory_controller #(
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH,
  parameter BID_SIZE                     = `BID_SIZE,
  parameter WORD_SIZE                    = `WORD_SIZE
)(
  /* verilator lint_off UNUSED */
  input  wire [ADDRESS_SIZE-1:0]                         i_addr,
  /* verilator lint_on UNUSED */
  input  wire                                            i_input_valid,

  input  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_flash_word_fetched_offset,
  input  wire [BID_SIZE-1:0]                             i_flash_output_bid,
  input  wire [ADDRESS_SIZE-1:0]                         i_flash_output_address,
  input  wire [WORD_SIZE-1:0]                            i_flash_word_output,
  input  wire                                            i_flash_instr_or_data,
  input  wire                                            i_flash_output_valid,

  input  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_ram_word_fetched_offset,
  input  wire [BID_SIZE-1:0]                             i_ram_output_bid,
  input  wire [ADDRESS_SIZE-1:0]                         i_ram_output_address,
  input  wire [WORD_SIZE-1:0]                            i_ram_word_output,
  input  wire                                            i_ram_instr_or_data,
  input  wire                                            i_ram_output_valid,

  output reg                                             o_flash_select,
  output reg                                             o_periperal_select,
  output reg                                             o_ram_select,

  output reg [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0]  o_mem_instruction_word_fetched_offset,
  output reg [BID_SIZE-1:0]                              o_mem_instruction_output_bid,
  output reg [WORD_SIZE-1:0]                             o_mem_instruction_word_output,
  output reg                                             o_mem_instruction_output_valid,

  output reg [ADDRESS_SIZE-1:0]                          o_mem_data_read_address,
  output reg [WORD_SIZE-1:0]                             o_mem_data_output,
  output reg                                             o_mem_data_output_valid
);

  always @(*)
  begin
    o_flash_select = 0;
    o_periperal_select = 0;
    o_ram_select = 0;

    if (i_input_valid)
      case(i_addr[ADDRESS_SIZE-1:ADDRESS_SIZE-2])
        `FLASH_START_OFFSET_PARTIAL:
        begin
          o_flash_select = 1;
        end
        `PERIPHERAL_START_OFFSET_PARTIAL:
        begin
          o_periperal_select = 1;
        end
        `RAM_START_OFFSET_PARTIAL:
        begin
          o_ram_select = 1;
        end
        default:
        begin
        end
      endcase
  end

  always @(*)
  begin
    o_mem_instruction_word_fetched_offset = 0;
    o_mem_instruction_output_bid = 0;
    o_mem_instruction_word_output = 0;
    o_mem_instruction_output_valid = 0;

    o_mem_data_read_address = 0;
    o_mem_data_output = 0;
    o_mem_data_output_valid = 0;

    if (i_ram_instr_or_data)
    begin
      o_mem_data_output = i_ram_word_output;
      o_mem_data_read_address = i_ram_output_address;
      o_mem_data_output_valid = 1;
    end
    else
    begin
      if (i_flash_instr_or_data)
      begin
        o_mem_data_output = i_flash_word_output;
        o_mem_data_read_address = i_flash_output_address;
        o_mem_data_output_valid = 1;
      end
      else
      begin
        if (i_ram_output_valid)
        begin
          o_mem_instruction_word_fetched_offset = i_ram_word_fetched_offset;
          o_mem_instruction_output_bid = i_ram_output_bid;
          o_mem_instruction_word_output = i_ram_word_output;
          o_mem_instruction_output_valid = 1;
        end
        else
        begin
          if (i_flash_output_valid)
          begin
            o_mem_instruction_word_fetched_offset = i_flash_word_fetched_offset;
            o_mem_instruction_output_bid = i_flash_output_bid;
            o_mem_instruction_word_output = i_flash_word_output;
            o_mem_instruction_output_valid = 1;
          end
        end
      end
    end
  end

endmodule

`endif // __MEMORY_CONTROLLER_MODULE__

