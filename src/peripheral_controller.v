/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __PERIPHERAL_CONTROLLER_MODULE__
`define __PERIPHERAL_CONTROLLER_MODULE__

`include "isa_const.v"
`include "processor_const.v"

// TODO: rewrite this using a standard bus (eg. Wishbone)

module peripheral_controller #(
  parameter ADDRESS_SIZE = `ADDRESS_SIZE
)(
  /* verilator lint_off UNUSED */
  input  wire [ADDRESS_SIZE-1:0] i_addr,
  /* verilator lint_on UNUSED */
  input  wire                    i_input_valid,
  output reg                     o_gpio_select,
  output reg                     o_uart_select
);

  always @(*)
  begin
    o_uart_select = 0;
    o_gpio_select = 0;

    if (i_input_valid)
      case(i_addr[ADDRESS_SIZE/2-1:ADDRESS_SIZE/2-2]) // FIXME
        `GPIO_START_OFFSET_PARTIAL:
        begin
          o_gpio_select = 1;
        end
        `UART_START_OFFSET_PARTIAL:
        begin
          o_uart_select = 1;
        end
        default:
        begin
        end
      endcase
  end

endmodule

`endif // __PERIPHERAL_CONTROLLER_MODULE__
