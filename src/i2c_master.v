`default_nettype none

`ifndef __I2C_MASTER_MODULE__
`define __I2C_MASTER_MODULE__

`include "to_array_max.v"

// This module is limited to a single master bus
module i2c_master #(
  parameter CLK_RATIO = 3,
  parameter SLAVE_ADDRESS_SIZE = 7,
  parameter BYTE_SIZE = 8
)(
  input  wire                            i_clk,
  input  wire [SLAVE_ADDRESS_SIZE+1-1:0] i_slave_address_rw,
  input  wire [BYTE_SIZE-1:0]            i_slave_data,
  input  wire                            i_input_valid,
  inout  wire                            io_i2c_scl,
  inout  wire                            io_i2c_sda
  /* output reg  [BYTE_SIZE-1:0]            o_read_data */
);

  localparam STATE_NB = 7;
  localparam IDLE        = 3'd0,
             START       = 3'd1,
             ADDRESS_RW  = 3'd2,
             ADDRESS_ACK = 3'd3,
             DATA        = 3'd4,
             DATA_ACK    = 3'd5,
             STOP        = 3'd6;

  reg  [$clog2(STATE_NB)-1:0] state = IDLE;

  reg [$clog2(BYTE_SIZE-1)-1:0] bit_counter = 0;

  // Clock generation
  wire                           clk;
  reg  [$clog2(CLK_RATIO-1)+1:0] q;

  // TODO: improve this frequency division (use a PLL?)
  always @(posedge i_clk)
    if (q == CLK_RATIO - 1)
      q <= 0;
    else
      q <= q + 1;

  assign clk = q == CLK_RATIO - 1;

  reg  odd_clock = 0;

  // I2C lines are active low
  reg  i2c_scl = 1;
  reg  i2c_sda = 1;

  assign io_i2c_scl = !i2c_scl ? i2c_scl : 1'bZ;
  assign io_i2c_sda = !i2c_sda ? i2c_sda : 1'bZ;

  /* verilator lint_off UNUSED */
  reg not_ack = 0;
  /* verilator lint_on UNUSED */

  always @(posedge clk)
    case(state)
      IDLE:
      begin
        i2c_scl <= 1'b1;
        i2c_sda <= 1'b1;
        not_ack <= 0;
        odd_clock <= 1;

        if (i_input_valid)
          state <= START;
      end
      START:
      begin
        odd_clock <= !odd_clock;

        if (odd_clock)
        begin
          i2c_sda <= 1'b0;
          bit_counter <= {$clog2(BYTE_SIZE){1'b0}};
          state <= ADDRESS_RW;
        end
      end
      ADDRESS_RW:
      begin
        odd_clock <= !odd_clock;

        if (odd_clock)
        begin
          i2c_scl <= !i2c_scl;

          // Send the address then the RW bit
          if (!i2c_scl)
          begin
            i2c_sda <= i_slave_address_rw[BYTE_SIZE - bit_counter - 1]; // MSB first
            bit_counter <= bit_counter + 1;
          end
        end

        // FIXME: check this condition
        if (bit_counter == `TO_ARRAY_MAX(BYTE_SIZE))
          state <= ADDRESS_ACK;
      end
      ADDRESS_ACK:
      begin
        i2c_scl <= !i2c_scl;

        // FIXME: change the condition to check for the ACK bit
        if (0)
        begin
          not_ack <= 1;
          state <= IDLE;
        end
        else
          state <= DATA;
      end
      DATA:
      begin
        odd_clock <= !odd_clock;

        if (odd_clock)
        begin
          i2c_scl <= !i2c_scl;
          // FIXME: obviously toggle the clock signal
          i2c_sda <= i_slave_data[BYTE_SIZE - bit_counter - 1]; // MSB first
          bit_counter <= bit_counter + 1;
        end

        // FIXME: check this condition
        if (bit_counter == `TO_ARRAY_MAX(BYTE_SIZE))
          state <= DATA_ACK;
      end
      DATA_ACK:
      begin
        i2c_scl <= !i2c_scl;
        // FIXME: change the condition to check for the ACK bit
        if (0)
        begin
          not_ack <= 1;
          state <= IDLE;
        end
        else
          // FIXME: add a condition for repeated START
          state <= STOP;
      end
      STOP:
      begin
        i2c_sda <= 1'b1;
        state <= IDLE;
      end
      default:
      begin
      end
    endcase

endmodule

`endif // __I2C_MASTER_MODULE__
