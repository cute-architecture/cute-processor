/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EXEC_NODE_INPUTS_DECODE_MODULE__
`define __EXEC_NODE_INPUTS_DECODE_MODULE__

`include "isa_const.v"

// TODO: maybe merge this module with exec_node_decode
module exec_node_inputs_decode #(
  parameter INSTRUCTION_SIZE = `INSTRUCTION_SIZE,
  parameter OPCODE_SIZE      = `OPCODE_SIZE,
  parameter GLOBAL_REG_NB    = `GLOBAL_REG_NB,
  parameter LOCAL_REG_NB     = `LOCAL_REG_NB
)(
  input  wire                               i_clk,
  input  wire                               i_reset,
  /* verilator lint_off UNUSED */
  input  wire [INSTRUCTION_SIZE-1:0]        i_instruction,
  /* verilator lint_on UNUSED */
  output reg  [OPCODE_SIZE-1:0]             o_opcode = 0,
  output reg  [$clog2(GLOBAL_REG_NB)-1:0]   o_addr_global = 0,
  output reg  [$clog2(LOCAL_REG_NB)-1:0]    o_addr_local = 0
);

  // FIXME: watch out for the null instruction (should only appear when the
  // instruction cache is not full)

  always @(posedge i_clk)
  begin
    if (i_reset)
    begin
      o_opcode <= 0;
      o_addr_global <= 0;
      o_addr_local <= 0;
    end
    case(i_instruction[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-OPCODE_SIZE])
      `READ_OP, `LDR_OP, `LDRB_OP:
      begin
        o_opcode <= i_instruction[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-OPCODE_SIZE];
        o_addr_global <= i_instruction[$clog2(GLOBAL_REG_NB)+$clog2(LOCAL_REG_NB)-1:$clog2(LOCAL_REG_NB)];
        o_addr_local <= i_instruction[$clog2(LOCAL_REG_NB)-1:0];
      end
      default:
      begin
      end
    endcase
  end

endmodule

`endif // __EXEC_NODE_INPUTS_DECODE_MODULE__
