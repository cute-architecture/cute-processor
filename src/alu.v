/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __ALU_MODULE__
`define __ALU_MODULE__

`include "isa_const.v"

// FIXME: support signed values

module alu #(
  parameter WORD_SIZE      = `WORD_SIZE,
  parameter OPCODE_SIZE    = `OPCODE_SIZE,
  parameter CONDITION_SIZE = `CONDITION_SIZE
)(
  input  wire                      i_clk,
  input  wire [WORD_SIZE-1:0]      i_input_a,
  input  wire [WORD_SIZE-1:0]      i_input_b,
  input  wire [OPCODE_SIZE-1:0]    i_opcode,
  input  wire [CONDITION_SIZE-1:0] i_condition,
  output reg  [WORD_SIZE-1:0]      o_result
);

  // TODO: add some more operations (eg. logical shifts)
  // TODO: test how operations behave when fed with negative values

  // FIXME: the ALU must provide a way for the program to detect overflows

  always @(posedge i_clk)
    case(i_opcode)
      `ADD_OP:
        o_result <= i_input_a + i_input_b;
      `ifndef REDUCE_LOGIC_USAGE
      `SUB_OP:
        o_result <= i_input_a - i_input_b;
      `endif
      `XOR_OP:
        o_result <= i_input_a ^ i_input_b;
      `AND_OP:
        o_result <= i_input_a & i_input_b;
      `ifdef HARDWARE_SHIFTS
      `LSL_OP:
        o_result <= i_input_b << i_input_a;
      `LSR_OP:
        o_result <= i_input_b >> i_input_a;
      `endif
      `ifndef REDUCE_LOGIC_USAGE
      `OR_OP:
        o_result <= i_input_a | i_input_b;
      `endif
      `CMP_OP:
      begin
        case(i_condition)
          `EQ_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b == i_input_a};
          `NE_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b != i_input_a};
          `ULT_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b <  i_input_a};
          `UGT_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b >  i_input_a};
          `ifndef REDUCE_LOGIC_USAGE
          `ULE_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b <= i_input_a};
          `UGE_COND:
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b >= i_input_a};
          `endif
          `SLT_COND:
            // FIXME: properly support signed values
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b <  i_input_a};
          `ifndef REDUCE_LOGIC_USAGE
          `SLE_COND:
            // FIXME: properly support signed values
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b <= i_input_a};
          `SGT_COND:
            // FIXME: properly support signed values
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b >  i_input_a};
          `SGE_COND:
            // FIXME: properly support signed values
            o_result <= {{(WORD_SIZE-1){1'b0}}, i_input_b >= i_input_a};
          `endif
          default:
          begin
          end
        endcase
      end
      default:
      begin
      end
    endcase

endmodule

`endif // __ALU_MODULE__
