/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`ifndef __ISA_CONST__
`define __ISA_CONST__

// Development
// `define REDUCE_LOGIC_USAGE

// Settings
// `define HARDWARE_SHIFTS

// Bit sizes
`define WORD_SIZE            32
`define BYTE_SIZE            8
`define INSTRUCTION_SIZE     (`WORD_SIZE)
`define ADDRESS_SIZE         32
`define OPCODE_SIZE          5
`define CONDITION_SIZE       4
`define CONDITION_JUMP_SIZE  3
`define IMMEDIATE_SIZE       16
`define SHORT_IMMEDIATE_SIZE 8
`define EXEC_MODE_SIZE       2
`ifdef REDUCE_LOGIC_USAGE
  `define BID_SIZE           14
`else
  `define BID_SIZE           28
`endif

// Memory map
// TODO: find a better solution to these _PARTIAL values
`define RAM_START_OFFSET                'h0000_0000 // FIXME
`define RAM_START_OFFSET_PARTIAL        'h0
`define PERIPHERAL_START_OFFSET         'h8000_0000 // FIXME
`define PERIPHERAL_START_OFFSET_PARTIAL 'h2
`define FLASH_START_OFFSET              'h4000_0000 // FIXME: set this to 0, and swap with RAM
`define FLASH_START_OFFSET_PARTIAL      'h1

// Block constraints
`define MAX_XNODE_COUNT              256 // Max number of execution node
`define LOCAL_REG_NB                 16 // STUDY
`define GLOBAL_REG_NB                256 // STUDY
`ifdef REDUCE_LOGIC_USAGE
  `define MAX_SUBBLOCK_NB            2 // Max number a block can branch to // STUDY
`else
  // `define MAX_SUBBLOCK_NB            16 // Max number a block can branch to // STUDY
  `define MAX_SUBBLOCK_NB            4 // Max number a block can branch to // STUDY
`endif
`define INSTR_MAX_SUBBLOCK_NB        16
`define MAX_BODY_LENGTH              64 // Maximum count of instructions per block // STUDY
`define MAX_LDR                      (`LOCAL_REG_NB) // STUDY
`define MAX_READ                     (`LOCAL_REG_NB) // STUDY
// FIXME: test if better
`ifdef REDUCE_LOGIC_USAGE
  `define MAX_STR                    2 // STUDY
  `define MAX_WRITE                  2 // STUDY
`else
  `define MAX_STR                    (`LOCAL_REG_NB) // STUDY
  `define MAX_WRITE                  (`LOCAL_REG_NB) // STUDY
`endif
`define MAX_INPUT_COUNT              (`LOCAL_REG_NB) // FIXME: this is max(MAX_LDR, MAX_READ)
`define MAX_INPUTS_BODY_BLOCK_LENGTH (`MAX_INPUT_COUNT + `MAX_BODY_LENGTH)

// Block input opcodes
// TODO: maybe change opcodes and reduce input opcode size if possible
// Opcodes
`define READ_OP  `OPCODE_SIZE'h1
`define LDR_OP   `OPCODE_SIZE'h2
`define LDRB_OP  `OPCODE_SIZE'h3

// Block body opcodes
// TODO: maybe change opcodes and reduce alu opcode size if possible
// Opcodes
`define ADD_OP   `OPCODE_SIZE'h11
`define SUB_OP   `OPCODE_SIZE'h12
`define XOR_OP   `OPCODE_SIZE'h14
`define AND_OP   `OPCODE_SIZE'h18
`define OR_OP    `OPCODE_SIZE'h13
`define LSL_OP   `OPCODE_SIZE'h5
`define LSR_OP   `OPCODE_SIZE'h6
`define MUL_OP   `OPCODE_SIZE'h15
`define CMP_OP   `OPCODE_SIZE'h1
`define BR_OP    `OPCODE_SIZE'h2
`define WRITE_OP `OPCODE_SIZE'h4
`define STR_OP   `OPCODE_SIZE'h3

// Condition codes
`define EQ_COND  `CONDITION_SIZE'h0
`define NE_COND  `CONDITION_SIZE'h1
`define ULT_COND `CONDITION_SIZE'h2
`define ULE_COND `CONDITION_SIZE'h3
`define UGT_COND `CONDITION_SIZE'h4
`define UGE_COND `CONDITION_SIZE'h5
`define SLT_COND `CONDITION_SIZE'h6
`define SLE_COND `CONDITION_SIZE'h7
`define SGT_COND `CONDITION_SIZE'h8
`define SGE_COND `CONDITION_SIZE'h9

// Execution mode codes
`define EXEC_MODE_NOEXEC `EXEC_MODE_SIZE'h0
`define EXEC_MODE_NOSPEC `EXEC_MODE_SIZE'h1
`define EXEC_MODE_LOOP   `EXEC_MODE_SIZE'h2
`define EXEC_MODE_SPEC   `EXEC_MODE_SIZE'h3

`endif // __ISA_CONST__
