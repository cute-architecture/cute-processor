/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __PROCESSOR_MODULE__
`define __PROCESSOR_MODULE__

`include "isa_const.v"
`include "processor_const.v"

`include "pack_unpack_2d_array.v"
`include "to_array_max.v"

module processor #(
  parameter GLOBAL_CLK_DIV               = `GLOBAL_CLK_DIV,
  parameter WORD_SIZE                    = `WORD_SIZE,
  parameter RAM_WORD_SIZE                = `RAM_WORD_SIZE,
  parameter RAM_ADDR_SIZE                = `RAM_ADDR_SIZE,
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter INSTRUCTION_SIZE             = `INSTRUCTION_SIZE,
  parameter BYTE_SIZE                    = `BYTE_SIZE,
  parameter EXECUTION_NODE_NB            = `EXECUTION_NODE_NB,
  parameter MAX_SUBBLOCK_NB              = `MAX_SUBBLOCK_NB, // Max number a block can branch to
  parameter IMMEDIATE_SIZE               = `IMMEDIATE_SIZE,
  parameter OPCODE_SIZE                  = `OPCODE_SIZE,
  parameter CONDITION_SIZE               = `CONDITION_SIZE,
  parameter CONDITION_JUMP_SIZE          = `CONDITION_JUMP_SIZE,
  parameter MAX_BODY_LENGTH              = `MAX_BODY_LENGTH,
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH,
  parameter BID_SIZE                     = `BID_SIZE,
  parameter EXEC_MODE_SIZE               = `EXEC_MODE_SIZE,
  parameter GLOBAL_REG_NB                = `GLOBAL_REG_NB,
  parameter LOCAL_REG_NB                 = `LOCAL_REG_NB,
  parameter MAX_STR                      = `MAX_STR,
  parameter MAX_WRITE                    = `MAX_WRITE,
  parameter GPIO_NB                      = 2,
  parameter SPI_ADDRESS_SIZE             = `SPI_ADDRESS_SIZE
)(
  input  wire                     i_clk,
  input  wire                     i_spi_miso,
  output wire                     o_spi_clk,
  output wire                     o_spi_mosi,
  output wire                     o_spi_ss,
  inout  wire [RAM_WORD_SIZE-1:0] io_sram_data,
  output wire [RAM_ADDR_SIZE-1:0] o_sram_addr,
  output wire                     o_sram_write_en_n,
  output wire                     o_sram_output_en_n,
  output wire                     o_sram_chip_select_n,
  output wire [GPIO_NB-1:0]       o_gpio,
  output wire [6-1:0]             o_scope,
  output wire                     o_uart_tx
);

  assign o_scope[6-2:0] = ram_word_output[6-2:0];
  assign o_scope[5] = ram_output_valid;

  // Execution nodes (EXN)
  wire [EXECUTION_NODE_NB-1:0]                    exn_discard_input;
  wire [EXEC_MODE_SIZE-1:0]                       exn_next_block_exec_mode [EXECUTION_NODE_NB-1:0];
  wire [BID_SIZE-1:0]                             exn_next_bid             [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_input_valid;
  wire [EXECUTION_NODE_NB-1:0]                    exn_next_block_index_inc;
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0]              exn_next_block_index     [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_next_block_valid;
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0]              exn_branch_block_index   [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_branch_block_index_valid;
  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] exn_instruction_to_fetch_offset [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_instruction_to_fetch_valid;
  wire [ADDRESS_SIZE-1:0]                         exn_mem_data_to_read_address [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_mem_data_to_read_valid;
  wire [BID_SIZE-1:0]                             exn_current_bid          [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_grant_global_reg_read;
  wire [EXECUTION_NODE_NB-1:0]                    exn_ready;
  wire [EXECUTION_NODE_NB-1:0]                    exn_request_global_reg_read;
  wire [$clog2(GLOBAL_REG_NB)-1:0]                exn_inputs_global_addr   [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]                    exn_current_block_ended;
  wire [EXECUTION_NODE_NB-1:0]                    exn_discard_subblocks;

  // Scheduler
  wire [$clog2(EXECUTION_NODE_NB)-1:0] exec_priority [EXECUTION_NODE_NB-1:0];
  wire [$clog2(EXECUTION_NODE_NB)-1:0] exec_depth;
  wire [$clog2(EXECUTION_NODE_NB)-1:0] current_exn;
  wire [BID_SIZE-1:0]                  selected_next_bid;
  wire                                 global_reg_write_en;
  wire [$clog2(GLOBAL_REG_NB)-1:0]     global_reg_addr;

  // WRITE FIFOs
  wire [$clog2(GLOBAL_REG_NB)+WORD_SIZE-1:0] write_fifo_output [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]               write_fifo_data_available;

  wire [$clog2(GLOBAL_REG_NB)+WORD_SIZE-1:0] write_fifo_output_muxed;
  wire                                       write_fifo_data_available_muxed;

  // STR FIFOs
  wire [ADDRESS_SIZE+WORD_SIZE-1:0] str_fifo_output [EXECUTION_NODE_NB-1:0];
  wire [EXECUTION_NODE_NB-1:0]      str_fifo_data_available;

  wire [ADDRESS_SIZE+WORD_SIZE-1:0] str_fifo_output_muxed;
  wire                              str_fifo_data_available_muxed;

  // Global register unit
  wire [WORD_SIZE-1:0] global_reg_output_data;

  // Memory controller
  wire                    flash_select;
  wire                    peripheral_select;
  /* verilator lint_off UNUSED */
  wire                    ram_select;
  /* verilator lint_on UNUSED */
  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] mem_block_offset;
  wire [ADDRESS_SIZE-1:0]                         mem_address;
  // Only the least significant byte is used for now
  // FIXME: rename this
  /* verilator lint_off UNUSED */
  wire [WORD_SIZE-1:0]                            mem_data;
  /* verilator lint_on UNUSED */
  wire [BID_SIZE-1:0]                             mem_input_bid;
  wire                                            mem_instr_or_data;
  /* verilator lint_off UNUSED */
  wire                                            mem_rw;
  /* verilator lint_on UNUSED */
  wire                                            mem_valid;
  // TODO: rename some of these signals
  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] mem_instruction_word_fetched_offset;
  wire [BID_SIZE-1:0]                             mem_instruction_output_bid;
  wire [WORD_SIZE-1:0]                            mem_instruction_word_output;
  wire                                            mem_instruction_output_valid;
  wire [WORD_SIZE-1:0]                            mem_data_read;
  wire [ADDRESS_SIZE-1:0]                         mem_data_read_address;
  wire                                            mem_data_read_valid;

  // Flash read controller
  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] flash_word_fetched_offset;
  wire [ADDRESS_SIZE-1:0]                         flash_word_address;
  wire [BID_SIZE-1:0]                             flash_output_bid;
  wire [WORD_SIZE-1:0]                            flash_word_output;
  wire                                            flash_instr_or_data;
  wire                                            flash_output_valid;

  // RAM
  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] ram_word_fetched_offset;
  wire [ADDRESS_SIZE-1:0]                         ram_word_address;
  wire [BID_SIZE-1:0]                             ram_output_bid;
  wire [WORD_SIZE-1:0]                            ram_word_output;
  wire                                            ram_instr_or_data;
  wire                                            ram_output_valid;

  // GPIO controller
  wire                 gpio_select;

  // UART
  wire                 uart_fifo_en_out;
  wire [BYTE_SIZE-1:0] uart_fifo_output;
  wire                 uart_fifo_data_available;
  wire                 uart_clk;
  wire                 uart_ready;
  wire                 uart_select;

  // Unpack signals
  wire [EXECUTION_NODE_NB*$clog2(EXECUTION_NODE_NB)-1:0] p_exec_priority;
  `UNPACK_ARRAY($clog2(EXECUTION_NODE_NB), EXECUTION_NODE_NB,
              exec_priority, p_exec_priority, unpack_i1)

  // Pack signals
  wire [EXECUTION_NODE_NB*$clog2(MAX_SUBBLOCK_NB)-1:0] p_exn_next_block_index;
  `PACK_ARRAY($clog2(MAX_SUBBLOCK_NB), EXECUTION_NODE_NB,
              exn_next_block_index, p_exn_next_block_index, pack_i0)

  wire [EXECUTION_NODE_NB*BID_SIZE-1:0] p_exn_next_bid;
  `PACK_ARRAY(BID_SIZE, EXECUTION_NODE_NB,
              exn_next_bid, p_exn_next_bid, pack_i1)

  wire [EXECUTION_NODE_NB*EXEC_MODE_SIZE-1:0] p_exn_next_block_exec_mode;
  `PACK_ARRAY(EXEC_MODE_SIZE, EXECUTION_NODE_NB,
              exn_next_block_exec_mode, p_exn_next_block_exec_mode, pack_i2)

  wire [EXECUTION_NODE_NB*$clog2(MAX_SUBBLOCK_NB)-1:0] p_exn_branch_block_index;
  `PACK_ARRAY($clog2(MAX_SUBBLOCK_NB), EXECUTION_NODE_NB,
              exn_branch_block_index, p_exn_branch_block_index, pack_i3)

  wire [EXECUTION_NODE_NB*$clog2(GLOBAL_REG_NB)-1:0] p_exn_inputs_global_addr;
  `PACK_ARRAY($clog2(GLOBAL_REG_NB), EXECUTION_NODE_NB,
              exn_inputs_global_addr, p_exn_inputs_global_addr, pack_i4)

  wire [EXECUTION_NODE_NB*BID_SIZE-1:0] p_exn_current_bid;
  `PACK_ARRAY(BID_SIZE, EXECUTION_NODE_NB,
              exn_current_bid, p_exn_current_bid, pack_i5)

  wire [EXECUTION_NODE_NB*$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] p_exn_instruction_to_fetch_offset;
  `PACK_ARRAY($clog2(MAX_INPUTS_BODY_BLOCK_LENGTH), EXECUTION_NODE_NB,
              exn_instruction_to_fetch_offset, p_exn_instruction_to_fetch_offset, pack_i6)

  wire [EXECUTION_NODE_NB*ADDRESS_SIZE-1:0] p_exn_mem_data_to_read_address;
  `PACK_ARRAY(ADDRESS_SIZE, EXECUTION_NODE_NB,
              exn_mem_data_to_read_address, p_exn_mem_data_to_read_address, pack_i7)

  wire slow_clk;
  reg  [$clog2(GLOBAL_CLK_DIV-1)-1:0] q;

  always @(posedge i_clk)
    if (q == `TO_ARRAY_MAX(GLOBAL_CLK_DIV))
      q <= 0;
    else
      q <= q + 1;

  assign slow_clk = q[$clog2(GLOBAL_CLK_DIV-1)-1] == 1;

  genvar k;
  for (k = 0; k < EXECUTION_NODE_NB; k = k + 1)
  begin: gen_exn
    exec_node #(
      .WORD_SIZE(WORD_SIZE),
      .INSTRUCTION_SIZE(INSTRUCTION_SIZE),
      .EXECUTION_NODE_NB(EXECUTION_NODE_NB),
      .MAX_SUBBLOCK_NB(MAX_SUBBLOCK_NB),
      .IMMEDIATE_SIZE(IMMEDIATE_SIZE),
      .OPCODE_SIZE(OPCODE_SIZE),
      .CONDITION_SIZE(CONDITION_SIZE),
      .CONDITION_JUMP_SIZE(CONDITION_JUMP_SIZE),
      .MAX_BODY_LENGTH(MAX_BODY_LENGTH),
      .MAX_INPUTS_BODY_BLOCK_LENGTH(MAX_INPUTS_BODY_BLOCK_LENGTH),
      .BID_SIZE(BID_SIZE),
      .EXEC_MODE_SIZE(EXEC_MODE_SIZE),
      .GLOBAL_REG_NB(GLOBAL_REG_NB),
      .LOCAL_REG_NB(LOCAL_REG_NB),
      .MAX_STR(MAX_STR),
      .MAX_WRITE(MAX_WRITE),
      .EXN_ID(k)
    ) exec_node (
      .i_clk(slow_clk),
      .i_discard(exn_discard_input[k]),
      .i_bid(selected_next_bid),
      .i_input_valid(exn_input_valid[k]),
      .i_exec_priority(exec_priority[k]),
      .i_exec_depth(exec_depth),
      .i_instruction_fetched(mem_instruction_word_output), // Flash memory reads are broadcast to all EXN
      .i_instruction_fetched_offset(mem_instruction_word_fetched_offset),
      .i_instruction_fetched_bid(mem_instruction_output_bid),
      .i_instruction_fetched_valid(mem_instruction_output_valid),
      .i_mem_data_read(mem_data_read),
      .i_mem_data_read_address(mem_data_read_address),
      .i_mem_data_read_valid(mem_data_read_valid),
      .i_next_block_index_inc(exn_next_block_index_inc[k]),
      .i_global_reg_data(global_reg_output_data),
      .i_grant_global_reg_read(exn_grant_global_reg_read[k]),
      .o_instruction_to_fetch_offset(exn_instruction_to_fetch_offset[k]),
      .o_instruction_to_fetch_valid(exn_instruction_to_fetch_valid[k]),
      .o_mem_data_to_read_address(exn_mem_data_to_read_address[k]),
      .o_mem_data_to_read_valid(exn_mem_data_to_read_valid[k]),
      .o_current_bid(exn_current_bid[k]), // TODO: check [k]
      .o_next_bid(exn_next_bid[k]),
      .o_next_block_exec_mode(exn_next_block_exec_mode[k]),
      .o_next_block_index(exn_next_block_index[k]),
      .o_next_block_valid(exn_next_block_valid[k]),
      .o_branch_block_index(exn_branch_block_index[k]),
      .o_branch_block_index_valid(exn_branch_block_index_valid[k]),
      .o_discard_subblocks(exn_discard_subblocks[k]),
      .o_is_ready(exn_ready[k]),
      .o_current_block_ended(exn_current_block_ended[k]),
      .o_request_global_reg_read(exn_request_global_reg_read[k]),
      .o_inputs_addr_global(exn_inputs_global_addr[k]),
      .o_write_fifo_output(write_fifo_output[k]),
      .o_write_fifo_data_available(write_fifo_data_available[k]),
      .o_str_fifo_output(str_fifo_output[k]),
      .o_str_fifo_data_available(str_fifo_data_available[k])
    );
  end

  assign write_fifo_output_muxed = write_fifo_output[current_exn];
  assign write_fifo_data_available_muxed = write_fifo_data_available[current_exn];

  assign str_fifo_output_muxed = str_fifo_output[current_exn];
  assign str_fifo_data_available_muxed = str_fifo_data_available[current_exn];

  scheduler_3xnodes #(
    .ADDRESS_SIZE(ADDRESS_SIZE),
    .EXECUTION_NODE_NB(EXECUTION_NODE_NB),
    .MAX_SUBBLOCK_NB(MAX_SUBBLOCK_NB),
    .MAX_INPUTS_BODY_BLOCK_LENGTH(MAX_INPUTS_BODY_BLOCK_LENGTH),
    .BID_SIZE(BID_SIZE),
    .GLOBAL_REG_NB(GLOBAL_REG_NB),
    .EXEC_MODE_SIZE(EXEC_MODE_SIZE)
  ) scheduler (
    .i_clk(slow_clk),
    .i_ready(exn_ready),
    .i_p_next_block_index(p_exn_next_block_index),
    .i_p_next_bid(p_exn_next_bid),
    .i_p_next_block_exec_mode(p_exn_next_block_exec_mode),
    .i_next_block_valid(exn_next_block_valid),
    .i_p_branch_block_index(p_exn_branch_block_index),
    .i_branch_block_index_valid(exn_branch_block_index_valid),
    .i_p_current_bid(p_exn_current_bid),
    .i_p_instruction_to_fetch_offset(p_exn_instruction_to_fetch_offset),
    .i_instruction_to_fetch_valid(exn_instruction_to_fetch_valid),
    .i_current_block_ended(| exn_current_block_ended), // TODO: maybe move this or operation inside scheduler
    .i_discard_subblocks(exn_discard_subblocks),
    .i_request_global_reg_read(exn_request_global_reg_read),
    .i_p_mem_data_to_read_address(p_exn_mem_data_to_read_address),
    .i_mem_data_to_read_valid(exn_mem_data_to_read_valid),
    .i_p_inputs_global_addr(p_exn_inputs_global_addr),
    .i_write_fifo_data_available_muxed(write_fifo_data_available_muxed),
    .i_str_fifo_output_muxed(str_fifo_output_muxed),
    .i_str_fifo_data_available_muxed(str_fifo_data_available_muxed),
    .o_mem_bid(mem_input_bid),
    .o_mem_block_offset(mem_block_offset),
    .o_mem_address(mem_address),
    .o_mem_data(mem_data),
    .o_mem_instr_or_data(mem_instr_or_data),
    .o_mem_rw(mem_rw),
    .o_mem_valid(mem_valid),
    .o_current_exn(current_exn),
    .o_p_exec_priority(p_exec_priority),
    .o_exec_depth(exec_depth),
    .o_selected_next_bid(selected_next_bid),
    .o_selected_next_valid(exn_input_valid),
    .o_next_block_index_inc(exn_next_block_index_inc),
    .o_discard_exn(exn_discard_input),
    .o_grant_global_reg_read(exn_grant_global_reg_read),
    .o_global_reg_write_en(global_reg_write_en),
    .o_global_reg_addr(global_reg_addr)
  );

  global_register_unit #(
    .WORD_SIZE(WORD_SIZE),
    .GLOBAL_REG_NB(GLOBAL_REG_NB)
  ) global_register_unit (
    .i_clk(slow_clk),
    .i_write_en(global_reg_write_en),
    .i_reg_read_addr(global_reg_addr),
    .i_write_fifo_output(write_fifo_output_muxed),
    .o_reg_output_data(global_reg_output_data)
  );

  memory_controller #(
    .ADDRESS_SIZE(ADDRESS_SIZE)
  ) memory_controller (
    .i_addr(mem_address), // FIXME: support read too
    .i_input_valid(mem_valid),
    .i_flash_word_fetched_offset(flash_word_fetched_offset),
    .i_flash_output_bid(flash_output_bid),
    .i_flash_output_address(flash_word_address),
    .i_flash_word_output(flash_word_output),
    .i_flash_instr_or_data(flash_instr_or_data),
    .i_flash_output_valid(flash_output_valid),
    .i_ram_word_fetched_offset(ram_word_fetched_offset),
    .i_ram_output_address(ram_word_address),
    .i_ram_output_bid(ram_output_bid),
    .i_ram_word_output(ram_word_output),
    .i_ram_instr_or_data(ram_instr_or_data),
    .i_ram_output_valid(ram_output_valid),
    .o_flash_select(flash_select),
    .o_periperal_select(peripheral_select),
    .o_ram_select(ram_select),
    .o_mem_instruction_word_fetched_offset(mem_instruction_word_fetched_offset),
    .o_mem_instruction_output_bid(mem_instruction_output_bid),
    .o_mem_instruction_word_output(mem_instruction_word_output),
    .o_mem_instruction_output_valid(mem_instruction_output_valid),
    .o_mem_data_read_address(mem_data_read_address),
    .o_mem_data_output(mem_data_read),
    .o_mem_data_output_valid(mem_data_read_valid)
  );

  flash_read_controller #(
    `ifdef VERILATOR
    .MOCK_MEMORY_FILE("../firmware/firmware.usr.hex"),
    `endif
    // .CLK_RATIO(2),
    .WORD_SIZE(WORD_SIZE),
    .FAST_READ_EN(0),
    .CPOL(1),
    .BID_SIZE(BID_SIZE),
    .EXECUTION_NODE_NB(EXECUTION_NODE_NB),
    .MAX_INPUTS_BODY_BLOCK_LENGTH(MAX_INPUTS_BODY_BLOCK_LENGTH)
  ) flash_read_controller (
    .i_clk(slow_clk),
    .i_bid(mem_input_bid),
    .i_word_block_offset(mem_block_offset),
    .i_word_address(mem_address),
    .i_input_instr_or_data(mem_instr_or_data),
    .i_input_valid(flash_select),
    .i_spi_miso(i_spi_miso),
    .o_spi_clk(o_spi_clk),
    .o_spi_mosi(o_spi_mosi),
    .o_spi_ss(o_spi_ss),
    .o_bid(flash_output_bid),
    .o_word_block_offset(flash_word_fetched_offset),
    .o_word_address(flash_word_address),
    .o_word_output(flash_word_output),
    .o_output_instr_or_data(flash_instr_or_data),
    .o_output_valid(flash_output_valid)
  );

  sram_controller #(
    .WORD_SIZE(WORD_SIZE),
    .RAM_WORD_SIZE(RAM_WORD_SIZE),
    .RAM_ADDR_SIZE(RAM_ADDR_SIZE)
  ) sram_controller (
    .i_clk(slow_clk),
    .i_bid(mem_input_bid),
    .i_word_block_offset(mem_block_offset),
    .i_addr(mem_address),
    .i_input_instr_or_data(mem_instr_or_data),
    .i_data(mem_data),
    .i_rw(mem_rw),
    .i_valid(ram_select),
    .io_sram_data(io_sram_data),
    .o_sram_addr(o_sram_addr),
    .o_sram_write_en_n(o_sram_write_en_n),
    .o_sram_output_en_n(o_sram_output_en_n),
    .o_sram_chip_select_n(o_sram_chip_select_n),
    .o_bid(ram_output_bid),
    .o_word_block_offset(ram_word_fetched_offset),
    .o_output_word(ram_word_output),
    .o_word_addr(ram_word_address),
    .o_output_instr_or_data(ram_instr_or_data),
    .o_output_valid(ram_output_valid)
  );

  peripheral_controller #(
    .ADDRESS_SIZE(ADDRESS_SIZE)
  ) peripheral_controller (
    // TODO: support reads someday
    .i_addr(mem_address),
    .i_input_valid(peripheral_select),
    .o_gpio_select(gpio_select),
    .o_uart_select(uart_select)
  );

  gpio_controller #(
    .BYTE_SIZE(BYTE_SIZE),
    .GPIO_NB(GPIO_NB)
  ) gpio_controller (
    .i_clk(slow_clk),
    .i_data(mem_data[BYTE_SIZE-1:0]),
    .i_input_valid(gpio_select),
    .o_gpio(o_gpio)
  );

  // FIXME: handle overflow for this FIFO (maybe reset it when
  // i_current_block_ended)
  // FIXME: add an o_full output
  fifo #(
    .DATA_WIDTH(BYTE_SIZE),
    .FIFO_LENGTH(MAX_STR)
  ) uart_fifo (
    .i_clk(slow_clk),
    .i_data_in(mem_data[BYTE_SIZE-1:0]),
    .i_en_in(uart_select),
    .i_en_out(uart_fifo_en_out),
    .i_reset(0),
    .o_data_out(uart_fifo_output),
    .o_data_available(uart_fifo_data_available),
    /* verilator lint_off PINCONNECTEMPTY */
    .o_last_item()
    /* verilator lint_on PINCONNECTEMPTY */
  );

  edge_detector #(
    .EDGE_TYPE(1)
  ) edge_detector (
    .i_clk(slow_clk),
    .i_signal(uart_clk && uart_ready),
    .o_edge_triggered(uart_fifo_en_out)
  );

  uart #(
    .CLK_DIV(`EXTERN_CLK_FREQ / GLOBAL_CLK_DIV / `UART_BITRATE),
    .WORD_SIZE(BYTE_SIZE),
    .PARITY_BIT(0),
    .STOP_LENGTH(1)
  ) uart (
    .i_clk(slow_clk),
    .i_input_tx(uart_fifo_output),
    .i_input_valid(uart_fifo_data_available),
    .o_uart_clk(uart_clk),
    .o_uart_tx(o_uart_tx),
    .o_is_ready(uart_ready)
  );

endmodule

`endif // __PROCESSOR_MODULE__
