/*
 * Copyright © Romain Fouquet, 2018-2021
 *
 * This file is part of The CUTE processor.
 *
 * The CUTE processor is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * The CUTE processor is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with The CUTE processor.  If not, see <https://www.gnu.org/licenses/>.
 */

`default_nettype none

`ifndef __EXEC_NODE_MODULE__
`define __EXEC_NODE_MODULE__

`include "isa_const.v"
`include "processor_const.v"
`include "to_array_max.v"

`define UPDATE_BRANCH_BLOCK_INDEX_VALID \
if (o_branch_block_index_valid) \
begin \
  /* Since the next block to execute is now known, it can be executed */ \
  o_next_block_index <= o_branch_block_index; \
  /* Extract BID from {EXEC MODE, BID} */ \
  o_next_bid <= footer_buffer[o_branch_block_index][BID_SIZE-1:0]; \
  /* Extract EXEC MODE from {EXEC MODE, BID} */ \
  /* o_next_block_exec_mode <= footer_buffer[o_branch_block_index][INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-EXEC_MODE_SIZE]; */ \
  o_next_block_exec_mode <= footer_buffer[o_branch_block_index][EXEC_MODE_SIZE+BID_SIZE-1:BID_SIZE]; \
  o_next_block_valid <= 1; \
end \
else \
  /* If this EXN's next block has been scheduled */ \
  if (i_next_block_index_inc) \
  begin \
    if (o_next_block_index != `TO_ARRAY_MAX(MAX_SUBBLOCK_NB)) \
    begin \
      o_next_block_index <= o_next_block_index + 1; \
      /* Extract BID from {EXEC MODE, BID} */ \
      o_next_bid <= footer_buffer[o_next_block_index + 1][BID_SIZE-1:0]; \
      /* Extract EXEC MODE from {EXEC MODE, BID} */ \
      o_next_block_exec_mode <= footer_buffer[o_next_block_index + 1][EXEC_MODE_SIZE+BID_SIZE-1:BID_SIZE]; \
      o_next_block_valid <= 1; \
    end \
    else \
      /* No more subblocks to speculatively execute */ \
      o_next_block_exec_mode <= `EXEC_MODE_NOEXEC; \
      o_next_block_valid <= 1; \
  end \
  else \
  begin \
    /* FIXME: this cannot work correctly when the next block reads data this */ \
    /* block writes (in memory or in registers) */ \
    o_next_bid <= footer_buffer[o_next_block_index][BID_SIZE-1:0]; \
    o_next_block_exec_mode <= footer_buffer[o_next_block_index][EXEC_MODE_SIZE+BID_SIZE-1:BID_SIZE]; \
    o_next_block_valid <= 1; \
  end \

`define DISCARD \
o_instruction_to_fetch_valid <= 0; \
o_discard_subblocks <= 1; \
// o_write_fifo_data_available <= 0; \
// o_str_fifo_data_available <= 0; \

`define REQUEST_NEXT_INSTRUCTION \
o_instruction_to_fetch_offset <= o_instruction_to_fetch_offset + 1; \
o_instruction_to_fetch_valid <= 1; \

`define FETCH_FOOTER \
if (footer_fetched) \
begin \
  `UPDATE_BRANCH_BLOCK_INDEX_VALID \
end \
else \
begin \
  /* If the taken branch is known */ \
  if (o_branch_block_index_valid) \
  begin \
    /* Request the taken subblock descriptor */ \
    /* Header + inputs + body + subblock offset */ \
    o_instruction_to_fetch_offset <= 1 \
    + {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count} \
    + body_length \
    + {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_SUBBLOCK_NB){1'b0}}, o_branch_block_index}; \
    o_instruction_to_fetch_valid <= 1; \
    loading_footer <= 1; \
  end \
  /* FIXME: fetch footer in advance when the branch index is not yet known, to allow */ \
  /* speculative execution */ \
end \

module exec_node #(
  parameter WORD_SIZE                    = `WORD_SIZE,
  parameter INSTRUCTION_SIZE             = `INSTRUCTION_SIZE,
  parameter ADDRESS_SIZE                 = `ADDRESS_SIZE,
  parameter BYTE_SIZE                    = `BYTE_SIZE,
  parameter MAX_XNODE_COUNT              = `MAX_XNODE_COUNT,
  parameter EXECUTION_NODE_NB            = `EXECUTION_NODE_NB,
  parameter MAX_SUBBLOCK_NB              = `MAX_SUBBLOCK_NB, // Max number a block can branch to
  parameter INSTR_MAX_SUBBLOCK_NB        = `INSTR_MAX_SUBBLOCK_NB,
  parameter IMMEDIATE_SIZE               = `IMMEDIATE_SIZE,
  parameter OPCODE_SIZE                  = `OPCODE_SIZE,
  parameter CONDITION_SIZE               = `CONDITION_SIZE,
  parameter CONDITION_JUMP_SIZE          = `CONDITION_JUMP_SIZE,
  parameter MAX_INPUT_COUNT              = `MAX_INPUT_COUNT,
  parameter MAX_BODY_LENGTH              = `MAX_BODY_LENGTH,
  parameter MAX_INPUTS_BODY_BLOCK_LENGTH = `MAX_INPUTS_BODY_BLOCK_LENGTH,
  parameter BID_SIZE                     = `BID_SIZE,
  parameter EXEC_MODE_SIZE               = `EXEC_MODE_SIZE,
  parameter GLOBAL_REG_NB                = `GLOBAL_REG_NB,
  parameter LOCAL_REG_NB                 = `LOCAL_REG_NB,
  parameter MAX_STR                      = `MAX_STR,
  parameter MAX_WRITE                    = `MAX_WRITE,
  parameter EXN_ID                       = 0
)(
  input  wire                                            i_clk,
  input  wire [BID_SIZE-1:0]                             i_bid,
  input  wire                                            i_input_valid,
  input  wire                                            i_discard,
  input  wire [$clog2(EXECUTION_NODE_NB)-1:0]            i_exec_priority,
  input  wire [$clog2(EXECUTION_NODE_NB)-1:0]            i_exec_depth,

  input  wire [INSTRUCTION_SIZE-1:0]                     i_instruction_fetched,
  input  wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] i_instruction_fetched_offset,
  input  wire [BID_SIZE-1:0]                             i_instruction_fetched_bid,
  input  wire                                            i_instruction_fetched_valid,

  input  wire [WORD_SIZE-1:0]                            i_mem_data_read,
  input  wire [ADDRESS_SIZE-1:0]                         i_mem_data_read_address,
  input  wire                                            i_mem_data_read_valid,

  input  wire                                            i_next_block_index_inc,

  input  wire [WORD_SIZE-1:0]                            i_global_reg_data,
  input  wire                                            i_grant_global_reg_read,

  // FIXME: replace to_fetch with request (everywhere)?
  output reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] o_instruction_to_fetch_offset = 0,
  output reg                                             o_instruction_to_fetch_valid = 0,

  // FIXME: drive these with LDR inputs
  output reg  [ADDRESS_SIZE-1:0]                         o_mem_data_to_read_address = 0,
  output reg                                             o_mem_data_to_read_valid = 0,

  output reg  [BID_SIZE-1:0]                             o_current_bid = 0,

  output reg  [BID_SIZE-1:0]                             o_next_bid = 0,
  output reg  [EXEC_MODE_SIZE-1:0]                       o_next_block_exec_mode = 0,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0]              o_next_block_index = 0,
  output reg                                             o_next_block_valid = 0,
  output reg  [$clog2(MAX_SUBBLOCK_NB)-1:0]              o_branch_block_index = 0,
  output reg                                             o_branch_block_index_valid = 0,

  output reg                                             o_discard_subblocks = 0,
  output reg                                             o_is_ready = 1,
  output reg                                             o_current_block_ended = 0,

  output reg                                             o_request_global_reg_read = 0,

  output wire [$clog2(GLOBAL_REG_NB)-1:0]                o_inputs_addr_global,

  output wire [$clog2(GLOBAL_REG_NB)+WORD_SIZE-1:0]      o_write_fifo_output,
  output reg                                             o_write_fifo_data_available = 0, // TODO: rename this to highlight this is updated only for the EXN with exec_priority == 0
  output wire [ADDRESS_SIZE+WORD_SIZE-1:0]               o_str_fifo_output,
  output reg                                             o_str_fifo_data_available = 0
);

  // TODO: try to use one hot encoding if this is in the critical path
  localparam STATE_NB = 8;
  localparam READY              = 3'd0,
             FETCHING_HEADER    = 3'd1,
             LOADING_INPUTS     = 3'd2,
             EXECUTING          = 3'd3,
             EXECUTION_COMPLETE = 3'd4,
             // STUDY
             // TODO: WRITE and STR could actually begin when state == EXECUTING if this EXN is the
             // current EXN
             COMMITING_WRITE    = 3'd5, // TODO: maybe swap COMMITING_STR and COMMITING_WRITE states because RAM access is way slower than register access, or make them parallel
             COMMITING_STR      = 3'd6,
             FETCHING_FOOTER    = 3'd7;

  reg  reset = 1;
  reg  loop = 0;
  reg  is_looping = 0;

  // Decode unit
  wire                             imm_sel;
  wire                             imm_sel_l;
  wire                             imm_upper_half;
  wire                             imm_upper_half_l;
  wire [IMMEDIATE_SIZE-1:0]        imm_value;
  wire [IMMEDIATE_SIZE-1:0]        imm_value_l;
  wire [$clog2(LOCAL_REG_NB)-1:0]  addr_a;
  wire [$clog2(LOCAL_REG_NB)-1:0]  addr_b;
  wire [$clog2(LOCAL_REG_NB)-1:0]  addr_d;
  wire [$clog2(LOCAL_REG_NB)-1:0]  addr_d_l;
  wire [$clog2(LOCAL_REG_NB)-1:0]  addr_d_l_l;
  wire [$clog2(GLOBAL_REG_NB)-1:0] addr_w;
  wire [$clog2(GLOBAL_REG_NB)-1:0] addr_w_l;

  wire                             inputs_end_reached;
  reg                              waiting_for_mem;
  reg                              waiting_for_global_reg;
  wire                             block_ended;
  reg                              block_ended_l;
  reg                              block_ended_l_l;
  reg                              block_ended_l_l_l;
  wire                             body_fetched;
  reg                              loading_footer = 0;
  // TODO: use a footer_fetched_flag so that the footer is not reloaded when looping
  reg                              footer_fetched = 0;

  // ALU
  wire [WORD_SIZE-1:0]               alu_input_a;
  wire [WORD_SIZE-1:0]               alu_input_b;
  wire [OPCODE_SIZE-1:0]             opcode;
  wire [OPCODE_SIZE-1:0]             opcode_l;
  // wire [OPCODE_SIZE-1:0]             opcode_l_l;
  wire [CONDITION_SIZE-1:0]          condition;
  wire [CONDITION_SIZE-1:0]          condition_l;
  `ifdef REDUCE_LOGIC_USAGE
  wire [$clog2(INSTR_MAX_SUBBLOCK_NB)-1:0] branch_block_index_f;
  /* verilator lint_off UNUSED */
  wire [$clog2(INSTR_MAX_SUBBLOCK_NB)-1:0] branch_block_index_f_l;
  /* verilator lint_on UNUSED */
  wire [$clog2(INSTR_MAX_SUBBLOCK_NB)-1:0] branch_block_index_t;
  /* verilator lint_off UNUSED */
  wire [$clog2(INSTR_MAX_SUBBLOCK_NB)-1:0] branch_block_index_t_l;
  /* verilator lint_on UNUSED */
  `else
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] branch_block_index_f;
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] branch_block_index_f_l;
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] branch_block_index_t;
  wire [$clog2(MAX_SUBBLOCK_NB)-1:0] branch_block_index_t_l;
  `endif
  wire [WORD_SIZE-1:0]               alu_result;

  // Register file
  wire                             reg_write_en;
  wire                             reg_write_en_l;
  wire                             reg_write_en_l_l;
  wire [WORD_SIZE-1:0]             reg_output_a;
  wire [WORD_SIZE-1:0]             reg_output_b;

  // WRITE FIFO
  wire                             write_fifo_in_en;
  wire                             write_fifo_in_en_l;
  wire                             write_fifo_data_available;
  wire                             write_fifo_last_item;
  reg                              write_fifo_last_item_l;

  // STR FIFO
  wire                             str_fifo_in_en;
  wire                             str_fifo_in_en_l;
  wire                             str_fifo_data_available;
  wire                             str_fifo_last_item;
  reg                              str_fifo_last_item_l;

  // Block descriptor
  reg  [EXEC_MODE_SIZE+BID_SIZE-1:0] footer_buffer [MAX_SUBBLOCK_NB-1:0];

  // Inputs decode unit
  wire [OPCODE_SIZE-1:0]          inputs_opcode;
  wire [$clog2(LOCAL_REG_NB)-1:0] inputs_addr_local;
  reg  [$clog2(LOCAL_REG_NB)-1:0] inputs_addr_local_l;

  // Instructions
  // FIXME: maybe reset this with the reset signal
  reg  [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] program_counter = 0;
  wire                                            requested_instruction_received;
  wire                                            instruction_buffer_write_en;
  wire [INSTRUCTION_SIZE-1:0]                     instruction;
  reg  [MAX_INPUTS_BODY_BLOCK_LENGTH-1:0]         instruction_fetched_flag = 0;
  wire                                            footer_buffer_write_en;

  reg  [$clog2(MAX_INPUT_COUNT)-1:0] input_count = 0;
  reg  [$clog2(MAX_BODY_LENGTH)-1:0] body_length = 0;
  /* verilator lint_off UNUSED */
  // TODO: use this
  reg  [$clog2(MAX_SUBBLOCK_NB)-1:0] footer_length = 0;
  /* verilator lint_on UNUSED */

  // FIXME: rename this
  reg  new_instruction = 0;
  reg  new_instruction_l = 0;

  // TODO: move this in proper modules
  always @(posedge i_clk)
  begin
    new_instruction_l <= new_instruction;

    if (reset || loop)
    begin
      block_ended_l <= 0;
      block_ended_l_l <= 0;
      block_ended_l_l_l <= 0;
    end
    else
    begin
      block_ended_l <= block_ended;
      block_ended_l_l <= block_ended_l;
      block_ended_l_l_l <= block_ended_l_l;
    end
  end

  wire ldr_op_reg_write_en;

  // TODO: maybe reduce the size of this to only MAX_BODY_LENGTH
  // wire [$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-1:0] jump_dest_offset;

  // FSM
  reg  [$clog2(STATE_NB)-1:0] state = READY;

  // TODO: find a cleaner solution to this
  reg  very_first_block_executed = 0;

  // FIXME: remove the need for these signals
  reg  first_instruction_executed = 0;
  reg  first_input_instruction_begun = 0;

  always @(posedge i_clk)
  begin
    str_fifo_last_item_l <= str_fifo_last_item;
    write_fifo_last_item_l <= write_fifo_last_item;
    inputs_addr_local_l <= inputs_addr_local;
  end

  // FIXME: rewrite this FSM with two processes
  always @(posedge i_clk)
  begin
    case(state)
      READY:
      begin
        reset <= 1;
        loop <= 0;
        is_looping <= 0;
        o_is_ready <= 1;
        o_request_global_reg_read <= 0;
        o_current_block_ended <= 0;
        o_discard_subblocks <= 0;
        program_counter <= 0;
        o_next_block_index <= 0;
        o_write_fifo_data_available <= 0;
        o_str_fifo_data_available <= 0;
        o_instruction_to_fetch_offset <= 0;
        o_instruction_to_fetch_valid <= 0;
        o_next_block_valid <= 0;
        o_mem_data_to_read_valid <= 0;
        // TODO: do not reset this in this state but in the next one, and try to make the scheduler
        // reschedule a block on a EXN where it is already fetched if possible
        // instruction_fetched_flag <= 0;
        loading_footer <= 0;
        waiting_for_mem <= 0;
        waiting_for_global_reg <= 0;
        new_instruction <= 0;
        first_instruction_executed <= 0;
        first_input_instruction_begun <= 0;

        // FIXME: check if there are other signals to reset

        // Start executing the first block
        if (!very_first_block_executed && EXN_ID == 0)
        begin
            very_first_block_executed <= 1;
            reset <= 0;
            o_is_ready <= 0;
            o_current_bid <= 0; // TODO: change the BID of the first block
            o_instruction_to_fetch_offset <= 0;
            o_instruction_to_fetch_valid <= 1;
            state <= FETCHING_HEADER;
        end
        else
        begin
          if (i_input_valid)
          begin
            reset <= 0;
            o_is_ready <= 0;
            o_current_bid <= i_bid;
            o_instruction_to_fetch_offset <= 0;
            o_instruction_to_fetch_valid <= 1;
            state <= FETCHING_HEADER;
          end
        end
      end
      FETCHING_HEADER:
      begin
        reset <= 0;

        if (requested_instruction_received)
        begin
          if ({{$clog2(MAX_XNODE_COUNT)-$clog2(EXECUTION_NODE_NB){1'b0}}, i_exec_depth}
            <= i_instruction_fetched[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-$clog2(MAX_XNODE_COUNT)])
          begin
            // TODO: move this decoding part into a separate, combinatorial module?
            // FIXME: do not use hard coded values
            input_count <= i_instruction_fetched[$clog2(MAX_INPUT_COUNT)+20-1:20];
            body_length <= i_instruction_fetched[$clog2(MAX_BODY_LENGTH)+13-1:13];
            footer_length <= i_instruction_fetched[$clog2(MAX_SUBBLOCK_NB)+9-1:9];

            `REQUEST_NEXT_INSTRUCTION
            // TODO: skip input loading if input_count == 0
            state <= LOADING_INPUTS;
          end
          else
          begin
            // FIXME: the block of which we just aborted execution is going to be rescheduled immediately
            o_instruction_to_fetch_valid <= 0;
            state <= READY;
          end
        end
      end
      LOADING_INPUTS:
      begin
        reset <= 0;
        o_request_global_reg_read <= 0;

        if (i_discard)
        begin
          `DISCARD
          state <= READY;
        end
        else
        begin
          o_is_ready <= 0;
          new_instruction <= 0;

          // TODO: pipeline input instruction fetching
          if (requested_instruction_received)
          begin
            `REQUEST_NEXT_INSTRUCTION
          end

          if (!first_instruction_executed && !first_input_instruction_begun
            && instruction_fetched_flag[0] && !(input_count == 0))
          begin
            o_request_global_reg_read <= 1;
            new_instruction <= 1;
            first_input_instruction_begun <= 1;
            waiting_for_global_reg <= 1;
          end

          if (inputs_end_reached && !waiting_for_global_reg && !waiting_for_mem
           && (first_instruction_executed || input_count == 0))
          begin
            // FIXME: explain why not reset o_instruction_fetch_valid
            state <= EXECUTING;
          end
          else
          begin
            if (ldr_op_reg_write_en)
            begin
              // Word from memory is written to the local register
              waiting_for_mem <= 0;
              o_mem_data_to_read_valid <= 0;
              first_instruction_executed <= 1;

              // If the next instruction has been fetched
              if (!inputs_end_reached && !waiting_for_global_reg && !waiting_for_mem
               && !i_mem_data_read_valid && instruction_fetched_flag[program_counter + 1])
              begin
                program_counter <= program_counter + 1;
                o_request_global_reg_read <= 1;
                new_instruction <= 1;
                waiting_for_global_reg <= 1;
              end
            end

            // If global register value is received
            if (i_grant_global_reg_read)
            begin
              o_request_global_reg_read <= 0;
              waiting_for_global_reg <= 0;
              if (inputs_opcode == `LDR_OP || inputs_opcode == `LDRB_OP)
              begin
                if (!ldr_op_reg_write_en)
                begin
                  // TODO: pipeline these requests as much as possible
                  // Request the word from memory whose address is
                  // the value of the global register
                  o_mem_data_to_read_address <= i_global_reg_data;
                  o_mem_data_to_read_valid <= 1;
                  waiting_for_mem <= 1;
                end
              end
              else
              begin
                first_instruction_executed <= 1;
              end
            end
            else
            begin
              // If the next instruction has been fetched
              if (!inputs_end_reached && !waiting_for_global_reg && !waiting_for_mem
               && !i_mem_data_read_valid && instruction_fetched_flag[program_counter + 1])
              begin
                program_counter <= program_counter + 1;
                o_request_global_reg_read <= 1;
                new_instruction <= 1;
                first_instruction_executed <= 1;
                waiting_for_global_reg <= 1;
              end
            end
          end
        end
      end
      EXECUTING:
      begin
        reset <= 0;
        loop <= 0;
        o_is_ready <= 0;
        o_request_global_reg_read <= 0;
        new_instruction <= 0;

        if (i_discard)
        begin
          `DISCARD
          state <= READY;
        end
        else
        begin
          if (body_fetched)
          begin
            o_instruction_to_fetch_valid <= 0;
            `FETCH_FOOTER
          end

          if (footer_fetched)
          begin
            `UPDATE_BRANCH_BLOCK_INDEX_VALID
          end

          if (first_instruction_executed && block_ended_l_l_l)
          begin
            // FIXME: check whether there is an extra instruction executed
            // TODO: it may be possible to skip this state it this EXN is the
            // current EXN
            o_instruction_to_fetch_valid <= 0;
            state <= EXECUTION_COMPLETE;
          end
          else
          begin
            // TODO: pipeline input instruction fetching
            if (!body_fetched && requested_instruction_received)
            begin
              `REQUEST_NEXT_INSTRUCTION
            end

            if (!first_instruction_executed && instruction_fetched_flag[0])
            begin
              first_instruction_executed <= 1;
              new_instruction <= 1;
            end

            // TODO: skip fetching instructions if we are going to jump over them anyway

            // Jump forward if needed, result of comparison is stored as the least significant bit
            // if (opcode_l_l == `CMP_OP && !alu_result[0])
            // begin
            //   // If the jump destination instruction has been fetched
            //   if (instruction_fetched_flag[jump_dest_offset])
            //   begin
            //     // Jump forward if the CMP condition was false
            //     program_counter <= jump_dest_offset;
            //     new_instruction <= 1;
            //   end
            // end
            // else
            // begin
            // If the next instruction has been fetched
            if (instruction_fetched_flag[program_counter + 1])
            begin
              program_counter <= program_counter + 1;
              new_instruction <= 1;
            end
            // end
          end
        end
      end
      EXECUTION_COMPLETE:
      begin
        o_is_ready <= 0;
        o_instruction_to_fetch_valid <= 0;

        if (i_discard)
        begin
          `DISCARD
          state <= READY;
        end
        else
        begin
          `FETCH_FOOTER

          if (footer_fetched)
          begin
            `UPDATE_BRANCH_BLOCK_INDEX_VALID
          end

          // If this EXN is executing the current block
          if (i_exec_priority == 0)
          begin
            if (write_fifo_data_available)
            begin
              o_write_fifo_data_available <= 1;
              state <= COMMITING_WRITE;
            end
            else
            begin
              if (str_fifo_data_available)
              begin
                o_str_fifo_data_available <= 1;
                state <= COMMITING_STR;
              end
              else
              begin
                if (footer_fetched)
                begin
                  // o_next_block_exec_mode is not used here because it may not yet be up-to-date
                  if (footer_buffer[o_branch_block_index][EXEC_MODE_SIZE+BID_SIZE-1:BID_SIZE] == `EXEC_MODE_LOOP)
                  begin
                    program_counter <= {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count};
                    o_next_block_valid <= 0;
                    first_instruction_executed <= 0;
                    new_instruction <= 0;
                    loop <= 1;
                    is_looping <= 1;
                    state <= EXECUTING;
                  end
                  else
                  begin
                    o_is_ready <= 1;
                    state <= READY;
                  end
                end
                else
                begin
                  state <= FETCHING_FOOTER;
                end
              end
            end
          end
        end
      end
      COMMITING_WRITE:
      begin
        o_is_ready <= 0;
        o_instruction_to_fetch_valid <= 0;

        `FETCH_FOOTER

        // FIXME: fix output of multiple values
        if (write_fifo_last_item_l)
        begin
          o_write_fifo_data_available <= 0;

          if (str_fifo_data_available)
          begin
            o_str_fifo_data_available <= 1;
            state <= COMMITING_STR;
          end
          else
          begin
            if (footer_fetched)
            begin
              `UPDATE_BRANCH_BLOCK_INDEX_VALID
              o_current_block_ended <= 1;
              o_is_ready <= 1;
              state <= READY;
            end
            else
            begin
              state <= FETCHING_FOOTER;
            end
          end
        end
      end
      COMMITING_STR:
      begin
        o_is_ready <= 0;
        o_instruction_to_fetch_valid <= 0;

        `FETCH_FOOTER

        // FIXME: fix output of multiple values
        if (str_fifo_last_item_l)
        begin
          o_str_fifo_data_available <= 0;

          if (footer_fetched)
          begin
            `UPDATE_BRANCH_BLOCK_INDEX_VALID
            o_current_block_ended <= 1;
            o_is_ready <= 1;
            state <= READY;
          end
          else
          begin
            state <= FETCHING_FOOTER;
          end
        end
      end
      FETCHING_FOOTER:
      begin
        if (footer_fetched)
        begin
          `UPDATE_BRANCH_BLOCK_INDEX_VALID

          // o_next_block_exec_mode is not used here because it may not yet be up-to-date
          if (footer_buffer[o_branch_block_index][EXEC_MODE_SIZE+BID_SIZE-1:BID_SIZE] == `EXEC_MODE_LOOP)
          begin
            program_counter <= {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count};
            o_next_block_valid <= 0;
            first_instruction_executed <= 0;
            new_instruction <= 0;
            loop <= 1;
            is_looping <= 1;
            state <= EXECUTING;
          end
          else
          begin
            o_is_ready <= 1;
            state <= READY;
          end
        end
        else
        begin
          `FETCH_FOOTER
        end
      end
      default:
      begin
        state <= READY;
      end
    endcase

    if (reset || loop)
    begin
      footer_fetched <= 0;
    end
    else
    begin
      if (footer_buffer_write_en)
      begin
        footer_buffer[i_instruction_fetched_offset
          - 1
          - {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count}
          - body_length] <= {i_instruction_fetched[INSTRUCTION_SIZE-1:INSTRUCTION_SIZE-EXEC_MODE_SIZE], i_instruction_fetched[BID_SIZE-1:0]};
        // FIXME: this works only if one subblock descriptor is to be fetched, i.e. the footer is
        // only fetched when the taken branch is known
        footer_fetched <= 1;
      end
    end
  end

  // assign jump_dest_offset = program_counter + 1
  //                       + {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-CONDITION_JUMP_SIZE{1'b0}},
  //                          condition_jump_l_l};

  assign inputs_end_reached =
    program_counter + 1 >= {{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}},
                            input_count};

  // Still compare to the maximum possible block length, in case the lengths in the block
  // header are wrong
  assign block_ended =
    (program_counter + 1 >= {{$clog2(MAX_BODY_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count} + body_length)
    || (program_counter == `TO_ARRAY_MAX(MAX_INPUTS_BODY_BLOCK_LENGTH));

  // Still compare to the maximum possible block length, in case the lengths in the block
  // header are wrong
  // FIXME: the last instruction being fetched does not mean all body instructions have been fetched
  // FIXME: this cannot work if fetching of the last instruction is skipped by a CMP instruction
  assign body_fetched =
    instruction_fetched_flag[{{$clog2(MAX_INPUTS_BODY_BLOCK_LENGTH)-$clog2(MAX_INPUT_COUNT){1'b0}}, input_count} + body_length - 1]
    || instruction_fetched_flag[MAX_INPUTS_BODY_BLOCK_LENGTH-1];

  // FIXME: is it better to also check if the instruction is not already in
  // the instruction_buffer, using instruction_fetched_flag
  // FIXME: simplify the check to FETCHING_HEADER
  assign requested_instruction_received = (!body_fetched || !footer_fetched
                                            || state == FETCHING_HEADER)
                                        &&  i_instruction_fetched_valid
                                        && (i_instruction_fetched_bid == o_current_bid)
                                        && (i_instruction_fetched_offset == o_instruction_to_fetch_offset);

  assign instruction_buffer_write_en = (state == LOADING_INPUTS || state == EXECUTING)
                                     && requested_instruction_received;

  assign footer_buffer_write_en = (!footer_fetched && loading_footer)
                                 && requested_instruction_received;

  always @(posedge i_clk)
  begin
    if (reset)
    begin
      instruction_fetched_flag <= 0;
    end
    else
    begin
      if (instruction_buffer_write_en)
      begin
        // FIXME: improve this comment/mechanism
        // - 1 is because of the block header
        instruction_fetched_flag[i_instruction_fetched_offset - 1] <= 1;
      end
    end
  end

  always @(posedge i_clk)
  begin
  end

  instruction_buffer #(
    .INSTRUCTION_SIZE(INSTRUCTION_SIZE),
    .BUFFER_LENGTH(MAX_INPUTS_BODY_BLOCK_LENGTH)
  ) instruction_buffer (
    .i_clk(i_clk),
    .i_write_en(instruction_buffer_write_en),
    .i_addr_r(program_counter),
     // FIXME: improve this comment/mechanism
     // - 1 is because of the block header
    .i_addr_w(i_instruction_fetched_offset - 1),
    .i_instruction(i_instruction_fetched),
    .o_instruction(instruction)
  );

  // TODO: enable this only during LOADING_INPUTS
  exec_node_inputs_decode #(
    .INSTRUCTION_SIZE(INSTRUCTION_SIZE),
    .OPCODE_SIZE(OPCODE_SIZE),
    .GLOBAL_REG_NB(GLOBAL_REG_NB),
    .LOCAL_REG_NB(LOCAL_REG_NB)
  ) exec_node_inputs_decode (
    .i_clk(i_clk),
    .i_reset(reset),
    .i_instruction(instruction),
    .o_opcode(inputs_opcode),
    .o_addr_global(o_inputs_addr_global),
    .o_addr_local(inputs_addr_local)
  );

  // TODO: enable this only during EXECUTING
  exec_node_decode #(
    .INSTRUCTION_SIZE(INSTRUCTION_SIZE),
    .OPCODE_SIZE(OPCODE_SIZE),
    .IMMEDIATE_SIZE(IMMEDIATE_SIZE),
    .GLOBAL_REG_NB(GLOBAL_REG_NB),
    .LOCAL_REG_NB(LOCAL_REG_NB),
    `ifdef REDUCE_LOGIC_USAGE
    .MAX_SUBBLOCK_NB(INSTR_MAX_SUBBLOCK_NB)
    `else
    .MAX_SUBBLOCK_NB(MAX_SUBBLOCK_NB)
    `endif
  ) exec_node_decode (
    .i_clk(i_clk),
    .i_reset(reset || loop),
    .i_en(state == EXECUTING && (new_instruction_l || (new_instruction && is_looping))),
    .i_instruction(instruction),
    .o_opcode(opcode),
    .o_imm_sel(imm_sel),
    .o_imm_upper_half(imm_upper_half),
    .o_imm_value(imm_value),
    .o_addr_a(addr_a),
    .o_addr_b(addr_b),
    .o_addr_d(addr_d),
    .o_condition(condition),
    .o_branch_block_index_f(branch_block_index_f),
    .o_branch_block_index_t(branch_block_index_t),
    .o_addr_w(addr_w),
    .o_reg_write_en(reg_write_en),
    .o_write_fifo_in_en(write_fifo_in_en),
    .o_str_fifo_in_en(str_fifo_in_en)
  );

  // TODO: remove these latches if possible and merge decode and register read
  // stages (because register addresses are aligned)
  exec_node_read_reg #(
    .GLOBAL_REG_NB(GLOBAL_REG_NB),
    .LOCAL_REG_NB(LOCAL_REG_NB),
    .IMMEDIATE_SIZE(IMMEDIATE_SIZE),
    .OPCODE_SIZE(OPCODE_SIZE),
    `ifdef REDUCE_LOGIC_USAGE
    .MAX_SUBBLOCK_NB(INSTR_MAX_SUBBLOCK_NB)
    `else
    .MAX_SUBBLOCK_NB(MAX_SUBBLOCK_NB)
    `endif
  ) exec_node_read_reg (
    .i_clk(i_clk),
    .i_opcode(opcode),
    .i_imm_sel(imm_sel),
    .i_imm_upper_half(imm_upper_half),
    .i_imm_value(imm_value),
    .i_condition(condition),
    .i_branch_block_index_f(branch_block_index_f),
    .i_branch_block_index_t(branch_block_index_t),
    .i_addr_d(addr_d),
    .i_write_en(reg_write_en),
    .i_addr_w(addr_w),
    .i_write_fifo_in_en(write_fifo_in_en),
    .i_str_fifo_in_en(str_fifo_in_en),
    .o_opcode(opcode_l),
    .o_imm_sel(imm_sel_l),
    .o_imm_upper_half(imm_upper_half_l),
    .o_imm_value(imm_value_l),
    .o_addr_d(addr_d_l),
    .o_condition(condition_l),
    .o_branch_block_index_f(branch_block_index_f_l),
    .o_branch_block_index_t(branch_block_index_t_l),
    .o_write_en(reg_write_en_l),
    .o_addr_w(addr_w_l),
    .o_write_fifo_in_en(write_fifo_in_en_l),
    .o_str_fifo_in_en(str_fifo_in_en_l)
  );

  exec_node_execute #(
    .REGISTER_NB(LOCAL_REG_NB),
    .IMMEDIATE_SIZE(IMMEDIATE_SIZE),
    .OPCODE_SIZE(OPCODE_SIZE),
    .CONDITION_JUMP_SIZE(CONDITION_JUMP_SIZE)
  ) exec_node_execute (
    .i_clk(i_clk),
    .i_addr_d(addr_d_l),
    .i_write_en(reg_write_en_l),
    .o_addr_d(addr_d_l_l),
    .o_write_en(reg_write_en_l_l)
  );

  // STUDY
  // Mux choosing between the immediate value and the register file output
  // Write bypass to avoid data hazard
  // TODO: this bypass (in input A) may be removed by forcing the assembler to
  // use the other register (on input B) when the previous instruction writes to
  // the same register that the current instruction reads (this must be enforced
  // for STR and WRITE instructions too)
  // /!\ With the current pipeline, extra instructions must be added as nop
  // before an instruction which uses register values written in the *two*
  // instructions before the current one
  // FIXME: for now disabling register forwarding
  // TODO: maybe add a processor feature flag to enable/disable upper half word
  // immediate value (because this may result in a lower maximum workinh
  // frequency: investigate this)
  assign alu_input_a = imm_sel_l ?
                       (imm_upper_half_l ?
                         {imm_value_l, {(WORD_SIZE-IMMEDIATE_SIZE){1'b0}}}:
                         {{(WORD_SIZE-IMMEDIATE_SIZE){1'b0}}, imm_value_l}):
                       // (addr_a == o_o_addr_d ? alu_result : reg_output_a); // FIXME: fix condition
                       reg_output_a;
  // STUDY
  // TODO: make the decode stage test register addresses, so that only a single
  // wire needs to be passed trough the register read stage (instead of
  // $clog2(LOCAL_REG_NB))
  // assign alu_input_b = addr_b == o_o_addr_d ? alu_result : reg_output_b; // FIXME: fix condition
  assign alu_input_b = reg_output_b;

  always @(posedge i_clk)
  begin
    if (reset || loop)
    begin
      o_branch_block_index_valid <= 0;
    end
    else
    begin
      if (opcode_l == `BR_OP)
      begin
        `ifdef REDUCE_LOGIC_USAGE
          o_branch_block_index <= alu_input_a[0] ? branch_block_index_t_l[$clog2(MAX_SUBBLOCK_NB)-1:0]
                                                 : branch_block_index_f_l[$clog2(MAX_SUBBLOCK_NB)-1:0];
        `else
          o_branch_block_index <= alu_input_a[0] ? branch_block_index_t_l : branch_block_index_f_l;
        `endif
        o_branch_block_index_valid <= 1;
      end
    end
  end

  alu #(
    .WORD_SIZE(WORD_SIZE),
    .OPCODE_SIZE(OPCODE_SIZE)
  ) alu (
    .i_clk(i_clk),
    .i_input_a(alu_input_a),
    .i_input_b(alu_input_b),
    .i_opcode(opcode_l),
    .i_condition(condition_l),
    .o_result(alu_result)
  );

  assign ldr_op_reg_write_en = i_mem_data_read_valid
                            && i_mem_data_read_address == o_mem_data_to_read_address;

  register_file #(
    .WORD_SIZE(WORD_SIZE),
    .REGISTER_NB(LOCAL_REG_NB)
  ) register_file (
    .i_clk(i_clk),
    // Prevent writing to L0
    // FIXME: add conditions for LDR
    // FIXME: rework these conditions
    .i_write_en(i_grant_global_reg_read ? inputs_addr_local_l != 0
                                        : (ldr_op_reg_write_en && waiting_for_mem) || ((state == EXECUTING || state == EXECUTION_COMPLETE) && reg_write_en_l_l && addr_d_l_l != 0) && new_instruction_l),
    .i_addr_a(addr_a),
    .i_addr_b(addr_b),
    .i_addr_d(i_grant_global_reg_read || ldr_op_reg_write_en ? inputs_addr_local : addr_d_l_l),
    .i_input_d(i_grant_global_reg_read ? i_global_reg_data
    : (ldr_op_reg_write_en && inputs_opcode == `LDR_OP ? i_mem_data_read
      : (ldr_op_reg_write_en && inputs_opcode == `LDRB_OP ? {{WORD_SIZE-BYTE_SIZE{1'b0}}, i_mem_data_read[WORD_SIZE-1:WORD_SIZE-BYTE_SIZE]}
        : alu_result))),
    .o_output_a(reg_output_a),
    .o_output_b(reg_output_b)
  );

  // WRITE FIFO
  // Store concatenations of a destination global register address and the data
  // word to be written there
  // TODO: maybe replace state == COMMITING_WRITE with i_exec_priority == 0
  // so that the FIFO can output as early as possible (but this is less
  // predictable)
  fifo #(
    .DATA_WIDTH($clog2(GLOBAL_REG_NB) + WORD_SIZE),
    .FIFO_LENGTH(MAX_WRITE)
  ) write_fifo (
    .i_clk(i_clk),
    .i_data_in({addr_w_l, alu_input_a}),
    .i_en_in(state == EXECUTING && write_fifo_in_en_l && new_instruction_l),
    .i_en_out(state == COMMITING_WRITE && write_fifo_data_available),
    .i_reset(reset || loop || i_discard), // TODO: maybe remove i_discard?
    .o_data_out(o_write_fifo_output),
    .o_data_available(write_fifo_data_available),
    .o_last_item(write_fifo_last_item)
  );

  // STR FIFO
  // Store concatenations of a main memory destination address and the data
  // word to be written there
  // TODO: maybe replace state == COMMITING_STR with i_exec_priority == 0
  // so that the FIFO can output as early as possible (but this is less
  // predictable)
  fifo #(
    .DATA_WIDTH(ADDRESS_SIZE+WORD_SIZE),
    .FIFO_LENGTH(MAX_STR)
  ) str_fifo (
    .i_clk(i_clk),
    .i_data_in({alu_input_b, alu_input_a}),
    .i_en_in(state == EXECUTING && str_fifo_in_en_l && new_instruction_l),
    .i_en_out(state == COMMITING_STR && str_fifo_data_available),
    .i_reset(reset || loop || i_discard), // TODO: maybe remove i_discard?
    .o_data_out(o_str_fifo_output),
    .o_data_available(str_fifo_data_available),
    .o_last_item(str_fifo_last_item)
  );

`ifdef FORMAL
  always @(posedge i_clk)
  begin
    // FIXME: what happens to o_instruction_to_fetch_offset when at the end of block?
    assert(program_counter <= o_instruction_to_fetch_offset);
  end
`endif
endmodule

`endif // __EXEC_NODE_MODULE__
