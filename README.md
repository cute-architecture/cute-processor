# The CUTE Processor

> The CUTE (CUTting Edge) architecture is intended to be a modern, innovative
ISA originally inspired by the EDGE architecture class.

## Building
#### Synthesis, Placement and Routing (PnR), Packaging and Flashing
Install [`yosys`](https://github.com/YosysHQ/yosys) (synthesis),
[`nextpnr`](https://github.com/YosysHQ/nextpnr) (PnR) and
`fpga-icestorm` (packaging and flashing).
`iceprogduino` is currently used to program the Flash chip of the FPGA
board by using an Arduino (beware of voltage levels, the FPGA board uses
3.3 V).
Optionally, install `entr` to be able to run the tests as soon as the files
change.
It may be useful to use the
[OLIMEXINO](https://www.olimex.com/Products/Duino/AVR/OLIMEXINO-32U4/),
which already has the appropriate connector and level converter built-in.

Use the following to run synthesis, routing, timing analysis and flashing:
```sh
make route && make pack && make time && make genfirmware && make flash
```

Use the following to only generate the new firmware and flash it:
```sh
make genfirmware FIRMWARE_FILE=test_opt && make flash
```

### Simulation
Install [`verilator`](https://veripool.org/wiki/verilator) (to run the
simulation) and `gtkwave` (to display generated digital waves).

### Architecture Graphs (optional)
Install [`netlistsvg`](https://github.com/nturley/netlistsvg) then
change the path in the `Makefile` to match you installation path.

### Generate the Documentation
Install XeTeX to generate a pdf file for this documentation.

## Documentation
See the `doc/` directory for documentation.

## License
HDL code is licensed under the LGPL 3.0 license or later, see `COPYING` and
`COPYING.LESSER`.
Documentation, logos and diagrams are licensed under
[CC BY-SA 4.0 International](https://creativecommons.org/licenses/by-sa/4.0/).
